<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="<?php echo $direction; ?>" lang="<?php echo $language; ?>" xml:lang="<?php echo $language; ?>">
<head>
<link type="image/x-icon" href="<?php echo HTTP_SERVER.DIR_IMAGE?>icon-yotab.png" rel="icon">
<link type="image/x-icon" href="<?php echo HTTP_SERVER.DIR_IMAGE?>icon-yotab.png" rel="shortcut icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!--<meta name="robots" content="INDEX,FOLLOW" />
<meta http-equiv="REFRESH" content="5400" />
<meta name="description" content="<?php echo $meta_description?>" />
<meta name="keywords" content="<?php echo $meta_keyword?>" />-->
<?php
	$str = str_replace(array("&lt;", "&gt;", '&amp;', '&#039;', '&quot;','&lt;', '&gt;'), array("<", ">",'&','\'','"','<','>'), htmlspecialchars_decode($this->document->setup['SEO'], ENT_NOQUOTES));
    echo $str;
?>
<title><?php echo $title?></title>
</head>

<!--[if lt IE 7]>
	<link href="<?php echo DIR_VIEW?>css/ie_style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?php echo DIR_VIEW?>js/ie_png.js"></script>
    <script type="text/javascript">
       ie_png.fix('.png');
	</script>
<![endif]-->
<link rel='stylesheet' type='text/css' href='<?php echo HTTP_SERVER.DIR_CSS ?>style_intro.css'>
<link rel='stylesheet' type='text/css' href='<?php echo HTTP_SERVER.DIR_CSS ?>blockui.css'>
<link rel='stylesheet' type='text/css' href='<?php echo HTTP_SERVER.DIR_CSS ?>jquery-ui.css'>
<link rel='stylesheet' type='text/css' href='<?php echo HTTP_SERVER.DIR_CSS ?>jquery.tabs.css'>
<link rel='stylesheet' type='text/css' href='<?php echo HTTP_SERVER.DIR_CSS ?>product.css'>
<script type='text/javascript' language='javascript' src='view/skin1/js/jquery.js'></script>
<script type='text/javascript' language='javascript' src='view/skin1/js/jquery.blockUI.js'></script>
<script type='text/javascript' language='javascript' src='view/skin1/js/maxheight.js'></script>
<script type='text/javascript' language='javascript' src='view/skin1/js/jquery.tabs.pack.js'></script>
<script type='text/javascript' language='javascript' src='view/skin1/js/common.js'></script>
<script type='text/javascript' language='javascript' src='view/skin1/js/cart.js'></script>
<body>
	<div id="ben-main">
		<?php echo $content ?>
    </div>
</div>
</body>
</html>
<div id="popup" style="display:none">
  <div id="popup-content"></div>
  <div>
    <input type="button" class="ben-button" value="<?php echo $button_close?>" onclick="closePopup()" />
  </div>
</div>