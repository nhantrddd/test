<div class="ben-post">
    <div id="login-form">
    	<div class="login-title">ĐĂNG NHẬP</div>
        <form id="frmLogin" method="post">
            <div id="error" class="ben-error" style="display:none"></div>
            <table width="100%">
                <tr>
                    <td width="83px;" class="ben-right"><label>Tên đăng nhập</label></td>
                    <td><input type="text" id="username" name="username" class="ben-textbox"></td>
                </tr>
                <tr>
                    <td class="ben-right"><label>Mật khẩu</label></td>
                    <td><input type="password" id="password" name="password" class="ben-textbox"></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                       <div style="text-align:left;">
                       <p>
                            <input type="button" class="ben-button" id="btnLogin" value="Đăng nhập"> <span style="color:#333;font-weight:900;"><a href="<?php echo $this->document->createLink('register') ?>">Đăng ký?</a></span>
                         </p>                  
                        <input type="checkbox" name="remember" value="1"> Ghi nhớ
                       
                        </div>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
<script language="javascript">
$("#btnLogin").click(function(){
	$.blockUI({ message: "<h1><?php echo $announ_infor ?></h1>" }); 
	
	$.post("<?php echo HTTP_SERVER?>?route=addon/login/login", $("#frmLogin").serialize(),
		function(data){
			if(data == "true")
			{
				alert("Bạn đã đăng nhập thành công!");
				if(history.length=1)
					window.location = '<?php echo HTTP_SERVER?>';
				history.go(-1);
			}
			else
			{
				
				$('#error').html(data).show('slow');
				
				
			}
			$.unblockUI();
		}
	);					   
});
</script>