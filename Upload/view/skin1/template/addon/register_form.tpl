<div class="ben-form-dk">
  	<div class="ben-form-top">
    	<div class="ben-left"><img src="<?php echo HTTP_SERVER.DIR_IMAGE ?>logo-dk.png" width="335" height="156" /></div>
        <div class="ben-left ben-line-dk"><img src="<?php echo HTTP_SERVER.DIR_IMAGE ?>line-form-dk.png" width="2" height="116" /></div>
        <div class="ben-left ben-title-form-dk">MÁY TÍNH BẢNG <br />100% TIẾNG VIỆT<br /> ĐẦU TIÊN<br /> TẠI VIỆT NAM</div>
        <div class="clearer"></div>
    </div>
    <div class="ben-content-form">
    	<div class="ben-content-form-top">
        	<div class="ben-title-content-form">ĐĂNG KÝ MUA HÀNG VỚI GIÁ SIÊU RẺ</div>
        </div>
        <div>
        <form id="frm">
        	<input type="hidden" name="productid" value="<?php echo $id ?>" />
            <div style="text-align:left;margin-top:5px;" class="ben-error ben-hidden"></div>
        	<table>
            	<tr>
                	<td style="vertical-align:middle; font-size:1.3em; text-align:right">Họ và tên (*)</td>
                    <td>
                    	<input class="ben-text-search" type="text" name="customername" value="">
                    </td>
                </tr>
                <tr>
                	<td style="vertical-align:middle; font-size:1.3em; text-align:right">Email (*)</td>
                    <td>
                    	<input class="ben-text-search" type="text" name="email" value="">
                    </td>
                </tr>
                <tr>
                	<td style="vertical-align:middle; font-size:1.3em; text-align:right">Điện thoại di động (*)</td>
                    <td>
                    	<input class="ben-text-search" type="text" name="phone" value="">
                    </td>
                </tr>
                <tr>
                	<td style="vertical-align:middle; font-size:1.3em; text-align:right">CMND (*)</td>
                    <td>
                    	<input class="ben-text-search" type="text" name="idcard" value="">
                    </td>
                </tr>
                <tr>
                	<td style="vertical-align:middle; font-size:1.3em; text-align:right">Địa chỉ (*)</td>
                    <td>
                    	<input class="ben-text-search" type="text" name="address" value="">
                    </td>
                </tr>
                <!--<tr>
                	<td style="vertical-align:middle; font-size:1.3em; text-align:right">Tỉnh/Thành phố (*)</td>
                    <td>
                    	<select tabindex="1" class="ben-select ben-left">
                        	<option value="1">TP.Hồ Chí Minh</option>
                            <option value="2">Hà Nội</option>
                            <option value="3">Bình Dương</option>
                            <option value="4">Bến Tre</option>
                            <option value="5">Cần Thơ</option>
                        </select>
                        <select tabindex="1" class="ben-select ben-left">
                        	<option value="1">chọn quận/huyện</option>
                            <option value="2">Tân Phú</option>
                            <option value="3">Tân Bình</option>
                            <option value="4">Q.1</option>
                            <option value="5">Q.2</option>
                        </select>
                        <div class="clearer"></div>
                    </td>
                </tr>-->
                <tr>
                	<td style="vertical-align:middle; font-size:1.3em; text-align:right">Ghi chú</td>
                    <td>
                    	<textarea style="width:383px; height:140px; border-radius:8px;" rows="5" cols="10" name="comment"></textarea>
                    </td>
                </tr>
                <tr>
                	<td style="vertical-align:middle; font-size:1.3em; text-align:right"></td>
                    <td>
                    	<div class="ben-left ben-btn-form"><a onclick="deleteData()"><img src="<?php echo HTTP_SERVER.DIR_IMAGE ?>btn-xoa.png" width="132" height="45" /></a></div>
                        <div class="ben-left ben-btn-form"><a onclick="register()"><img src="<?php echo HTTP_SERVER.DIR_IMAGE ?>btn-dk.png" width="132" height="45" /></a></div>
                        
                        <div class="clearer"></div>
                        <div class="ben-ttbb">(*)Thông tin bắt buộc</div>
                    </td>
                </tr>
            </table>
        </form>
        </div>
        
    </div>
  </div>
  
  <div id="popup" style="display:none">
  <div id="popup-content"></div>
  <div>
    <input type="button" class="ben-button" value="<?php echo $button_close?>" onclick="closePopup()" />
  </div>
</div>

<script language="javascript">
function register(){
	$.post("?route=addon/saleoff/orderRegister",$("#frm").serialize(),
		function(data){
			if(data=="true"){
				alert("Bạn đã đăng ký mua hàng thành công!");
				window.location.reload();	
			}
			else{
				$('.ben-error').html(data);
				$('.ben-error').fadeIn("slow");
			}	
		}
	);
		
}

function deleteData(){
	$('#frm').each(function(){
	  this.reset();
	});
}
</script>