<?php if($popup['attr'] !== false): ?>
    <link href="<?php echo HTTP_SERVER; ?>bPopup/style.css" type="text/css" rel="stylesheet" />
    <script src="<?php echo HTTP_SERVER; ?>bPopup/script1.js" type="text/javascript"></script>
    <div class="bOverlay" id="bPopup">
        <div class="bPopup bSize" style="<?php echo (isset($popup['attr']['width']) && (int)$popup['attr']['width'] > 0) ? ('width:' . $popup['attr']['width'] . 'px !important;'): ''; ?> <?php echo (isset($popup['attr']['height']) && (int)$popup['attr']['height'] > 0) ? ('height:' . $popup['attr']['height'] . 'px !important;'): ''; ?>">
            <div class="bClose" title="Đóng" style="<?php echo (isset($popup['attr']['width']) && (int)$popup['attr']['width'] > 0) ? ('margin-left:' . $popup['attr']['width'] . 'px !important;'): ''; ?>"></div>
            <div class="bContent bSize" style="<?php echo (isset($popup['attr']['width']) && (int)$popup['attr']['width'] > 0) ? ('width:' . $popup['attr']['width'] . 'px !important;'): ''; ?> <?php echo (isset($popup['attr']['height']) && (int)$popup['attr']['height'] > 0) ? ('height:' . $popup['attr']['height'] . 'px !important;'): ''; ?>">
                <!--BEGIN: Content-->
                <?php echo htmlspecialchars_decode($popup['detail']); ?>
                <!--END Content-->
            </div>
        </div>
    </div>
    <?php if(!isset($_SESSION["bPopupShowed"]) || (isset($_SESSION["bPopupShowed"]) && isset($popup['attr']['number']) && $_SESSION["bPopupShowed"] < $popup['attr']['number']) || (isset($popup['attr']['number']) && $popup['attr']['number'] < 1)): ?>
        <?php
        if(!isset($_SESSION["bPopupShowed"]))
        {
            $_SESSION["bPopupShowed"] = 1;
        }
        else
        {
            $_SESSION["bPopupShowed"] = $_SESSION["bPopupShowed"] + 1;
        }
        ?>
        <script language="javascript">bPopup();</script>
    <?php endif; ?>
<?php endif; ?>