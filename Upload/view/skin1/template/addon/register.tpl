<script src='<?php echo HTTP_SERVER.DIR_JS?>ui.datepicker.js' type='text/javascript' language='javascript'> </script>
<div class="ben-post">
<div class="form-register">
	<div class="login-title">ĐĂNG KÝ THÀNH VIÊN</div>
    <div id="error" class="ben-error" style="display:none"></div>
	<form id="frmRegister" method="post">
        <div class="table-register">
    	<table id="register-form" width="100%">
        	<tr>
            	<td><label>Tên đăng nhập</label></td>
                <td><input type="text" id="username" name="username" class="ben-textbox" size="40"></td>
            </tr>
            <tr>
            	<td><label>Mật khẩu</label></td>
                <td><input type="password" id="password" name="password" class="ben-textbox" size="40"></td>
            </tr>
            <tr>
            	<td><label>Xác nhận mật khẩu</label></td>
                <td><input type="password" id="confirmpassword" name="confirmpassword" class="ben-textbox" size="40"></td>
            </tr>
            <tr>
            	<td><label>Họ và tên</label></td>
                <td><input type="text" id="fullname" name="fullname" class="ben-textbox" size="40"></td>
            </tr>
            <tr>
            	<td><label>Email</label></td>
                <td><input type="text" id="email" name="email" class="ben-textbox" size="40"></td>
            </tr>
            <tr>
            	<td><label>Địa chỉ</label></td>
                <td><input type="text" id="address" name="address" class="ben-textbox" size="40"></td>
            </tr>
            <tr>
            	<td><label>Điện thoại</label></td>
                <td><input type="text" id="phone" name="phone" class="ben-textbox" size="40"></td>
            </tr>
            <tr>
            	<td><label>Ngày sinh</label></td>
                <script language="javascript">
					$(function() {
						$("#birthday").datepicker({
								changeMonth: true,
								changeYear: true,
								dateFormat: 'dd-mm-yy'
								});
						});
				 </script>
                <td><input type="text" id="birthday" name="birthday" class="ben-textbox" size="40"></td>
            </tr>
            <tr>
            	<td></td>
                <td><div class="dieukhoan">Khách hàng cần điền đầy đủ thông tin liên hệ chính xác để thuận tiện cho việc mua sản phẩm tại website Yotab. Sau khi đăng ký tài khoản, chúng tôi sẽ gửi email xác nhận đến địa chỉ mail của khách hàng để kích hoạt tài khoản. Địa chỉ khách hàng cần phải được ghi đầy đủ và chính xác để việc giao hàng tận nơi được thuận tiện.</div></td>
            </tr>
            <tr>
            	<td></td>
                <td>
                	<input type="checkbox" id="chkaccept" name="chkaccept" value="accept" class="ben-textbox">
                    Tôi đồng ý với điều khoản trên
                </td>
            </tr>
            <tr>
            	<td></td>
                <td>
                	<input type="button" id="btnRegister" name="btnRegister" class="ben-button" value="Đăng ký">
                </td>
            </tr>
        </table>
        </div>
    </form>
    </div>
</div>
<div class="clearer">&nbsp;</div>
<script language="javascript">
$("#btnRegister").click(function(){
	$.blockUI({ message: "<h1><?php echo $announ_infor ?></h1>" }); 
	
	$.post("<?php echo HTTP_SERVER?>?route=addon/register/save", $("#frmRegister").serialize(),
		function(data){
			if(data == "true")
			{
				$('#error').html("Bạn đã đăng ký thành công! Mã kích hoạt tài khoản đã được gửi tới email của bạn!").show('slow');
				$("#frmRegister").hide();
			}
			else
			{
				
				$('#error').html(data).show('slow');
				
				
			}
			$.unblockUI();
		}
	);					   
});
</script>