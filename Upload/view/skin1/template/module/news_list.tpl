<div>
	<div class="ben-tintuc">
    	<?php
        if(count($medias)){
        ?>
        <div>
            <div class="ben-left"><a href="<?php echo $medias[0]['link'] ?>">
            	<img src="<?php echo $medias[0]['imagepreview'] ?>" /></a></div>
            <div class="ben-left">
                <div class="ben-title-tt">
                    <a href="<?php echo $medias[0]['link'] ?>" class="ben-a-title-tt"><?php echo $medias[0]['title'] ?></a>
                </div>
                <div class="ben-text-tt">
                    <?php echo htmlspecialchars_decode($medias[0]['summary']) ?>
                </div>
                
                <div class="clearer"></div>
          </div>
            <div class="clearer"></div>
        </div>
        <?php
        }
        ?>
        <div class="ben-content-tintuc">
        	<?php
            if(count($medias)){
            	for($i = 1; $i < count($medias); $i++){
            ?>
            <div class="ben-line-nx2">
                <div class="ben-left"><a href="<?php echo $medias[$i]['link'] ?>">
                	<img src="<?php echo $medias[$i]['imagethumbnail'] ?>" /></a></div>
                <div class="ben-left ben-bg-tt">
                    <label style="color:#eb008b;font-size:20px;font-weight:bold;">
                    <a href="<?php echo $medias[$i]['link'] ?>" class="ben-a-title-tt">
                    	<?php echo $medias[$i]['title'] ?>
                    </a></label>
                    <?php echo htmlspecialchars_decode($medias[$i]['summary']) ?>
                    
                    <div class="clearer"></div>
                </div>

                <div class="clearer"></div>
            </div>
            <?php
            	}
            }
            ?>
            
            <div class="ben-sltrang ben-pager">
                <div class="ben-left">
                    <?php echo $prevlink ?>
                </div>
                <div class="ben-right">
                    <?php echo $nextlink ?>
                </div>
                <div class="clearer"></div>
            </div>
        </div>
  </div>
</div>
<script language="javascript">
	$("#ancarat_sort").val("<?php echo $_GET['order'] ?>");
	$("#show_item").val("<?php echo $_GET['to'] ?>");
	
	function fun_show_item(){
		window.location = '?to=' + $('#show_item_btn').val() + '&order=' + $('#ancarat_sort').val();
	}
</script>