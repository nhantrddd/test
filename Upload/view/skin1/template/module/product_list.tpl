<div class="ben-left listproduct">
    <div id="dListDeal" class="travel-container">    
        <div class="deals">        
            <div class="gr-deal gr-deal-3">   
            <?php
            	if(count($medias)){
                	foreach($medias as $media){
            ?>         
                <section class="deal">    
                <?php if($media['saleoff'] > 0){ ?>            
                	<div class="giamgia"> <span>-<?php echo $media['saleoff'] ?>%</span> </div>                
                <?php } ?>
                	<div class="deal-img"> 
                    	<a href="<?php echo $media['link'] ?>" title="<?php echo $media['title'] ?>">
                        	<img src="<?php echo $media['imagethumbnail'] ?>" />
                                
		                <?php if($media['saleoff'] > 0){ ?>
                            <span class="deal-sale-Tour deal-price"><s><?php echo $this->string->numberFormate($media['priceold']) ?></s><em><?php echo $this->document->setup['Currency']?></em></span> 
                        <?php } ?>
                        </a> 
                        <span class="discount"><?php echo $this->string->numberFormate($media['price']) ?> <em><?php echo $this->document->setup['Currency']?></em></span> 
                    </div>                
                	<h1><a href="<?php echo $media['link'] ?>" title="<?php echo $media['title'] ?>" class="deal-title">
                    	<?php echo $this->string->getNumberTextLength($media['title'], 0, 6) ?>
                    </a></h1>                
                    <div class="deal-rate">                    
                    	<div class="luotmua"><b>
                        	<?php echo $this->string->numberFormate($media['soldout']) ?>
                        </b> lượt mua</div>         
                        <a href="<?php echo $media['link'] ?>">           
                    	<div class="deal-time"> 
                            <img src="<?php echo HTTP_SERVER.DIR_IMAGE ?>icon-ct.png" width="10" height="10" />&nbsp;&nbsp;Chi tiết
                        </div>                    
                        </a>
                    </div>                
                </section> 
            <?php
            		}
            	}
            ?>
            </div>
        </div>
    </div>
</div>
<div class="clearer"></div>
<div><?php echo $pager ?></div>