<div>
      <div class="ben-wrapper">
      	<!--<div class="sitemap">
        	<?php echo $this->document->breadcrumb?> » <?php echo $media['productname'] ?>
        </div>-->
        <div class="summary-detail-sp">
        <div class="summary-detail-sp-content">
        	<div class="ben-left thanh-image-detail">
            	<div class="product-mainimage">
                    <table>
                        <tr valign="middle">
                            <td id="product-preview"><a class="lightbox" href="<?php echo $subimage[0]['imagepreview']?>" rel="flowers"><img src="<?php echo $subimage[0]['imagethumbnail']?>" /></a></td>
                        </tr>
                    </table>
                    
                </div>        
                <div id="ben-galary" class="ben-center">
                    <table>
                        <tr>
                            <td style="width:14px">
                                <img class="ben-link" id="ben-prev" src="<?php echo HTTP_SERVER.DIR_IMAGE?>galary_button_prev.png" />
                            </td>
                            <td>
                                <div style="overflow:hidden;margin:0 auto;">
                                    <table id="ben-scroll">
                                        <tr>
                                           
                                            <?php 
                                            	if(count($subimage)){
                                                	foreach($subimage as $key => $val){ 
                                            ?>
                                            <td>
                                                <img  id="icon-<?php echo $key?>" class="ben-icon-item" src="<?php echo $val['imageicon']?>" />
                                            </td>
                                            <?php 
                                            		}
                                                } 
                                            ?>
                                        </tr>
                                    </table>
                                </div>
                                
                            </td>
                            <td style="width:14px">
                                <img class="ben-link" id="ben-next" src="<?php echo HTTP_SERVER.DIR_IMAGE?>galary_button_next.png" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="display:none">
                    <?php foreach($subimage as $key => $val){ ?>
                    <div  id="preview-<?php echo $key?>">
                        <a class="lightbox" href="<?php echo $val['imagepreview']?>" rel="flowers"><img src="<?php echo $val['imagethumbnail']?>" /></a>
                    </div>
                    <?php } ?>
                </div>

            </div>
            <div class="ben-left summary-sp">
            	<div class="title-summary">
                	<h2><span class="title-detail"><?php echo $media['productname'] ?></span></h2>
                </div>
                
                <!--<div class="summary-detail">
                	<table class="data-summary-sp">
                    	<tr>
                        	<th>Tính năng nổi bật</th>
                            <th>Giá sản phẩm</th>
                        </tr>
                        <tr>
                        	<td>
                            	<p>
                                	Hãng sản xuất: <a href="<?php echo $this->document->createLink('manufacturer',$media['manufacturerid'])?>"><?php echo $media['manufacturername']?></a>
                                </p>
								<?php echo htmlspecialchars_decode($media['summary']) ?>
                            </td>
                            <td>
                            	<div class="gia-showroom">
                                	<h5>Giá tại showroom (đã bao gồm VAT)</h5>
                                    
                                    <div>
                                    	<?php if($media['price']==0){ ?>
                                        <div class="price-big ben-left">Liên hệ</div>
                                        <?php }else{ ?>
                                       	<div class="price-big ben-left"><?php echo $this->string->numberFormate($media['price'])?></div>&nbsp;<div class="price ben-left"><?php echo $this->document->setup['Currency']?></div>
                                        <?php }?>
                                    	
                                        <div class="clearer"></div>
                                    </div>
                                    <div>
                                    	<div class="ben-left" style="margin-top:7px">Số lượng</div>
                                        <div class="ben-left"><input type="text" class="text-soluong" id="soluong" /></div>
                                        <div class="ben-left">
       <input type="button" class="mua" onclick="cart.add('<?php echo $media['id']?>',$('#soluong').val())" />
                                        </div>
                                        <div class="clearer"></div>
                                    </div>
                                </div>
                                <div class="khuyenmai">
                                	<div class="price">KHUYẾN MÃI</div>
                                    <?php echo htmlspecialchars_decode($media['sale']) ?>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>-->
               <!--	<div class="tuvan">
                	<?php echo htmlspecialchars_decode($tuvanmuahang['description']);?>
                    <div class="clearer"></div>
                </div>-->
                <div class="summary-detail">
                	<!--<span class="text-bold"><strong>Giá tại Showroom (đã bao gồm VAT)</strong></span>-->
                    <div class="price-detail">
                    	<div>
                        	<strong>Giá thị trường:</strong> <?php echo $this->string->numberFormate($media['priceold']) ?> 
                        	<?php echo $this->document->setup['Currency']?>
                        </div>
                    	<div>
                        	<strong>Giá bán:</strong> <?php echo $this->string->numberFormate($media['price']) ?> 
                        	<?php echo $this->document->setup['Currency']?>
                            (Tiết kiệm: <?php echo $media['saleoff'] ?>%)
                        </div>
                        <div>
                        	<strong>Ngày khởi hành:</strong> 
                            <?php echo $this->date->formatMySQLDate($media['startdate']) ?> 
                        </div>
                    </div>
                
                <br />
                <div class="soluong">
                    <span class="text-bold">Số lượng:</span> <input type="text" size="5" class="ben-textbox" id="soluong" name="qty" value="1"/>
                    <div class="soluong-img"><a onclick="cart.add('<?php echo $media['id']?>',$('#soluong').val())"><img src="<?php echo HTTP_SERVER.DIR_IMAGE?>btn-muangay.png" /></a></div>
                </div>
                </div>
 				<br />
                <div class="summary-detail">
                	<?php echo htmlspecialchars_decode($media['summary']) ?>
                </div>
 				<!--<div class="khuyenmai">
                	<span class="sale">KHUYẾN MÃI:</span> <?php echo htmlspecialchars_decode($media['sale']) ?>
             	</div>-->
            </div>
            <div class="clearer">&nbsp;</div>
            </div>
        </div>
      </div>
    </div>

    <div class="thanh-tab-content ben-left">
        <div id="ben-insidecontent">
            <div id="container">
                    <ul >
                        <li class="tabs-selected"><a href="#fragment-chitiettinhnang"><span>Chi tiết</span></a></li>
                        <li><a href="#fragment-baiviet"><span>Bài viết</span></a></li>
                        <!--<li><a href="#fragment-sanphamcungloai"><span>Các tour gợi ý</span></a></li>-->
                        <!--<li><a href="#fragment-baohanh"><span>Bảo hành</span></a></li>-->
                    </ul>
                    <div id="fragment-chitiettinhnang">     
                    	<?php echo htmlspecialchars_decode($media['description']) ?>
                    </div> 
                    <div id="fragment-sanphamcungloai">
                   		  <?php echo $saphamcungloai ?>
                    </div>
                    
                     <div id="fragment-baiviet">
                   		  <?php echo htmlspecialchars_decode($media['post']) ?>
                    </div>
                    
                    <div id="fragment-baohanh">
                   		<?php echo htmlspecialchars_decode($media['warranty']) ?>
                    </div>
                    
               </div> 
        	
        </div>
        
    </div>
    

    <!--Tư vấn mua hàng-->
    <div class="ben-left">
        <div class="tu-van-mua-hang">
            <?php echo htmlspecialchars_decode($tuvanmuahang) ?>
        </div>
        <div class="tu-van-mua-hang">
            <div>
				<p style="text-align: center; padding: 10px;">
					<span style="font-size:20px;"><span style="color: rgb(255, 0, 0);">
                    	<strong>Các Tour Gợi Ý</strong>
                    </span></span>
        		</p>
            	<div style="margin:0 auto;width: 250px;">
                	<?php echo $saphamcungloai ?>
                </div>
            </div>

        </div>
    </div>
    <div class="clearer">&nbsp;</div>
    <!--NHẬN XÉT KHÁCH HÀNG-->
    <!--<div class="comment">
    	<div class="comment-title">
        	<H5>NHẬN XÉT CỦA KHÁCH HÀNG</H5>
        </div>
        <div class="comment-content">
        	
            <?php foreach($comments as $comment) {?>
            <div class="cm-detail">
            
                <p>
                    <b><?php echo $comment['fullname'] ?></b>&nbsp; <?php echo $this->date->formatMySQLDate($comment['commentdate'],'longdate') ?>
                </p>
                
                <div class="comment-detail">
                    <p>
                        <?php echo $comment['description'] ?>
                    </p>
                    <p class="ben-right"><b><?php echo $comment['email'] ?></b></p>
                    <div class="clearer"></div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>-->
    
    <!--Ý KIẾN CỦA BẠN-->
    <!--<div class="comment">
    	<div class="comment-title">
        	<H5>Ý KIẾN CỦA BẠN</H5>
        </div>
        <div class="ykien-content thanh-tab-content">
         <div id="error" class="ben-error" style="display:none"></div>
        	<form name="frm-ykien" id="frm-ykien" method="post" enctype="multipart/form-data">
            <input type="hidden" name="mediaid" id="mediaid" value="<?php echo $this->request->get['id'] ?>"  />
            <input type="hidden" name="refersitemap" id="refersitemap" value="<?php echo $media['sitemapid'] ?>" />
            <table>
            	<tr>
                	<td>(*) Họ Tên</td>
                    <td><div style="padding-left:3px;"><input class="text-comment" type="text" name="fullname" /></div></td>
                    <td>(*) Email</td>
                    <td><input class="text-comment" type="text" name="email" /></td>
                </tr>
                <tr>
                	<td>(*) Nội dung</td>
                    <td colspan="3"><textarea class="comment2" name="description"></textarea></td>
                </tr>
                <tr>
                	<td>&nbsp;</td>
                    <td><input type="button" id="guiykien" class="guiykien" /></td>
                    <td>&nbsp;</td>
                    <td>(*)Thông tin bắt buộc</td>
                </tr>
            </table>
            </form>
        </div>
    </div>-->

<script language="javascript">
  	$(document).ready(function(){
			$('#container').tabs({ fxSlide: false, fxFade: true, fxSpeed: 'slow' });
			
			//Gửi ý kiến
			$('#guiykien').click(function(e) {
				$.blockUI({ message: "<h1>Please wait...</h1>" }); 
                var url="?route=module/comment/sendComment";
				$.post(url,$('#frm-ykien').serialize(),function(data){
						if(data=="true")
						{
							alert("Gởi nhận xét thành công");
							$.unblockUI();
							window.location.reload();
						}
						else
						{
						
						$('#error').html(data).show('slow');
						
						$.unblockUI();
						}
					
					
				});
				
            });
			
			
			
			
			
			
			
			
			
		
		});
</script> 



<script language="javascript">
$(document).ready(function() { 
$(".ben-icon-item").click(function(){
	var arr = this.id.split("-");
	var key = arr[1];
	
	$("#product-preview").html($("#preview-"+key).html());
	$(".lightbox").lightbox();
});
$('#ben-container').tabs({ fxSlide: true, fxFade: true, fxSpeed: 'slow' });
	
});
</script>
<script language="javascript">
var block1 = 62;
var length = $("#ben-scroll").width();
var view = 248;
var move = true
var movenext = true
$("#ben-prev").click(function(){
	movenext = true;
	if(move == true)
	{
		move = false;
		var left = $("#ben-scroll").css("margin-left").replace("px","");
		//alert(left);
		if(left < 0)
		{
			
			$("#ben-scroll").animate({"marginLeft": "+="+ block1 +"px"}, "slow",function(){
				/*left = $("#ben-scoll").css("margin-left").replace("px","");
				if(left > 0)
					$("#ben-scoll").css("margin-left","0px");*/
				move = true;
			});
			
			
		}
		else
			move = true;
	}
		
});
$("#ben-next").click(function(){
	length = $("#ben-scroll").width();
	if(movenext == true)
	{
		movenext = false;
		
		var left = $("#ben-scroll").css("margin-left").replace("px","");
		
		if( parseInt(length) + parseInt(left) > view)
			$("#ben-scroll").animate({"marginLeft": "-="+ block1 +"px"}, "slow",function(){
				movenext = true;
			});
	}
	
});
</script>

<script language="javascript">
$(".product-icon").click(function(){
	var arr = this.id.split("-");
	var key = arr[1];
	$("#product-preview").attr("src",$("#preview-"+key).attr("src"));
});
</script>