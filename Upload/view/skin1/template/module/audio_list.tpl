<script language="javascript" type="text/javascript" src="<?php echo HTTP_SERVER.DIR_JS ?>swfobject.js" ></script>
<?php echo HTTP_SERVER.DIR_JS ?>
<!-- Div that contains player. -->
<div id="player">
<h1>No flash player!</h1>
<p>It looks like you don't have flash player installed. <a href="http://www.macromedia.com/go/getflashplayer" >Click here</a> to go to Macromedia download page.</p>
</div>

<!-- Script that embeds player. -->
<script language="javascript" type="text/javascript">
var so = new SWFObject("<?php echo HTTP_SERVER.DIR_COMPONENT ?>flashmp3player.swf", "player", "290", "247", "9"); // Location of swf file. You can change player width and height here (using pixels or percents).
so.addParam("quality", "high");
so.addVariable("content_path","<?php echo HTTP_SERVER.DIR_FILE ?>upload"); // Location of a folder with mp3 files (relative to php script).
so.addVariable("color_path","default.xml"); // Location of xml file with color settings.
so.addVariable("script_path","<?php echo HTTP_SERVER.DIR_CONTROLLER ?>module/flashmp3player.php"); // Location of php script.
so.write("player");
</script>
