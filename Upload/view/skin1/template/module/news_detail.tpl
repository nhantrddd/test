<div class="ben-left">
	<div class="ben-title-ctsp"><?php echo $post['title']?></div>
    <div class="ben-section-content-ctsp">
	    <?php echo htmlspecialchars_decode($post['description']) ?>
    </div>

    <div class="othernews-content">
    <?php if($othernews) {?>
    	<div class="ben-hline"></div>
    	<h3><div class="othernews">Các tin khác</div></h3>             
        <div>
            <ul>
            <?php foreach($othernews as $media) {?>
            	<li><a href="<?php echo $media['link']?>" class="ben-othernews">
                	<?php echo $media['title']?>&nbsp;
                    <span class="ben-other">(<?php echo $media['statusdate']?>)</span>
                </a></li>
            <?php } ?>    
            </ul>
        </div>
    <?php } ?>    
    <div class="clearer">&nbsp;</div>
    </div>
</div>