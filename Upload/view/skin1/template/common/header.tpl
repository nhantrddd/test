<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38054862-1']);
  _gaq.push(['_setDomainName', 'dulichhot.com.vn']);
  _gaq.push(['_setAllowLinker', true]);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<style>
.ben-banner-header {
    float: left;
    margin: 15px 0 0 275px;
}
</style>
<div id="ben-header">
    <!--Header top-->
    <div id="ben-top">
    	<!--lOGO-->
        <div id="ben-top-header">
        	<div id="ben-logo" class="ben-left">
            	<a href="<?php echo HTTP_SERVER ?>">
            		<img src="<?php echo HTTP_SERVER.DIR_IMAGE ?>logo-dulich.png" width="209" height="51" />
            	</a>
            </div>
            <div class="ben-banner-header">
                <a href="http://dulichhot.com.vn/tour-du-lich/tour-lan-bien.html" target="_blank">
                    <embed src="<?php echo HTTP_SERVER.DIR_IMAGE ?>banner-lan.swf" width="550" height="65"></embed>
                </a>
            </div>
            <div class="ben-right ben-nn">            
                <div class="ben-left ben-vn">
                	<div class="ben-left"><a href="<?php echo HTTP_SERVER ?>">Tiếng Việt</a>&nbsp;&nbsp;&nbsp;</div>                
                	<div class="ben-left">
                    	<img src="<?php echo HTTP_SERVER.DIR_IMAGE ?>vn.png" width="21" height="14" />
                    </div>                
                	<div class="clearer"></div>
                </div>
                <div class="ben-left">                
                	<div class="ben-left">
                    	<img src="<?php echo HTTP_SERVER.DIR_IMAGE ?>eng.png" width="21" height="14" />
                    </div>                
                	<div class="ben-left">&nbsp;&nbsp;&nbsp;<a href="#">English</a></div>
                    <div class="clearer"></div>                
                </div>            
            	<div class="clearer"></div>            
            </div>        
        	<div class="clearer"></div>        
        </div>    
        
        <!--MENU-->
        <div class="ben-navigation">        
            <ul id="ben-main-nav">  
            	<?php echo $mainmenu ?>
            </ul>        
        	<div class="clearer">&nbsp;</div>        
        </div>    
    </div>
    
    <!--Header content-->
    <div id="ben-content-header">
    	<div id="ben-content-header-search" class="ben-left"> 
    		<?php echo $searchtour ?>
        </div>
        
        <!--sssssssssssssssssssssssssssssssssssssss-->
        <div id="ben-content-header-khuyenmai" class="ben-right">
        	<?php echo $bannerhome ?>
        </div>    
    	<div class="clearer">&nbsp;</div>    
    </div>
</div>
<script language="javascript">
	$(document).ready(function() {
		$("#tour-du-lich-child a").hover(
			function(){
				$("#tour-du-lich").addClass('tour-du-lich-parent');
			}
		);
	});
	function tour_close(){
		$("#tour-du-lich").removeClass('tour-du-lich-parent');
	}
</script>