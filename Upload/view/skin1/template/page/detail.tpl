<div class="ben-section page-detail">
    <?php
    if($sitemap['sitemapname'] != ''){
    ?>
        <div class="ben-bg-title">
            <div>
                <p style="color:#929497"><?php echo $this->document->breadcrumb ?></p>
            </div>
            <div class="ben-tt">
                <?php 
                    echo $sitemap['sitemapname'];
                ?>
            </div>
        </div>       
    <?php
    }
    ?>
    <div class="ben-section-content">
        <?php echo $module ?>
    </div>
</div>