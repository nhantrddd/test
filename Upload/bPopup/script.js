function bPopup()
{
    $('#bPopup').fadeIn();
    var h = (($(window).height()) * 1 - ($('.bPopup').height() * 1)) / 2;
    $('.bPopup').css('margin-top', h);
}

$(document).ready(function() {
    $('#bPopup .bClose').click(function() {
        $('#bPopup').fadeOut();
    });
    $(document).keydown(function(e) {
        var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
        switch (key) {
            case 27:
                $('#bPopup').fadeOut();
                break;
        }
    });
});