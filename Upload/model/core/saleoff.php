<?php
$this->load->model("core/file");
class ModelCoreSaleoff extends ModelCoreFile
{ 
	public function getItem($id, $where="")
	{
		$sql="Select `user_saleoff`.* 
									from `user_saleoff` 
									where id ='".$id."' ".$where;
		$query = $this->db->query($sql);
		return $query->row;
	}
	
	public function getList($where="", $from=0, $to=0)
	{
		
		$sql = "Select `user_saleoff`.* 
									from `user_saleoff` 
									where status <> 'delete' " . $where ;
		if($to > 0)
		{
			$sql .= " Limit ".$from.",".$to;
		}
		
		$query = $this->db->query($sql);
		return $query->rows;
	}
	
	
	//update table product - field purchasenumber
	public function updateCol($id,$col,$val)
	{
		$id= $this->db->escape(@$id);
		$col= $this->db->escape(@$col);
		$val= $this->db->escape(@$val);
		
		
		
		$field=array(
						$col
					);
		$value=array(
						$val
					);
		
		$where="id = '".$id."'";
		$this->db->updateData("product",$field,$value,$where);
		
		return true;
	}
	
	public function insert($data)
	{
		$productid = $this->db->escape(@$data['productid']);
		$parentproduct =$this->db->escape(@$data['parentproduct']);
		$customername=$this->db->escape(@$data['customername']);
		$idcard=$this->db->escape(@$data['idcard']);
		$phone=$this->db->escape(@$data['phone']);
		$address=$this->db->escape(@$data['address']);
		$purchaseprice=$this->db->escape(@$data['purchaseprice']);
		$purchasedate=$this->db->escape(@$data['purchasedate']);
		$email=$this->db->escape(@$data['email']);
		$status='new';
		$comment=$this->db->escape(@$data['comment']);
		
		$field=array(
						'`productid`',
						'`parentproduct`',
						'`customername`',
						'`idcard`',
						'`phone`',
						'`address`',
						'`purchaseprice`',
						'`purchasedate`',
						'`email`',
						'`status`',
						'`comment`'
						
					);
		$value=array(
						$productid,
						$parentproduct,
						$customername,
						$idcard,
						$phone,
						$address,
						$purchaseprice,
						$purchasedate,
						$email,
						$status,
						$comment,
					);
		$this->db->insertData("user_saleoff",$field,$value);
	}
	
	
	
	public function delete($id)
	{
		$id=$this->db->escape(@$id);
		//Xoa nhung file dinh kem
		/*$user_saleoff = $this->getItem($id);
		if($user_saleoff['attachment']!="")
		{
			$list = split(",",$user_saleoff['attachment']);
			foreach($list as $item)
				$this->deleteFile($item);
		}*/
		//Xoa tin nhan
		if($id != "")
		{
			$sql = "Update `user_saleoff` set status='delete' where id = '".$id."'";
			$this->db->query($sql);
		}
	}
	
	private function senduser_saleoff($data)
	{
		$id=$this->db->escape(@$data['id']);
		$username=$this->db->escape(@$data['username']);
		$senddate=$this->date->getToday();
		$field=array(
						'id',
						'username',
						'status',
						'folder',
						'senddate'
					);
		$value=array(
						$id,
						$username,
						"",
						"inbox",
						$senddate
					);
		$this->db->insertData("user_saleoffsend",$field,$value);
	}
	
	public function sendEmail($data)
	{
		$to=$data['email'];
		// subject
		$subject = $data['title'];
		// user_saleoff
		$user_saleoff = $data['description'];
		
		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		
		// Additional headers
		$headers .= 'From: '.$data['from'];
							
		// Mail it
		@mail($to, $subject, $user_saleoff, $headers);
	}
	
	private function getTarget($to)
	{
		$listarrdress = split(",",$to);
		$listuser = array();
		$listemail = array();
		//Loc ra danh sach username va danh sach email
		foreach($listarrdress as $item)
		{
			$mystring = trim($item);
			$findme   = '@';
			$pos = strpos($mystring, $findme);
			if ($pos === false) 
			{
				//Not found
				$listuser[]=$this->processString($item);
			} 
			else 
			{
				//found
				$listemail[]=$this->processString($item);
			}
		}
		$data['listuser'] = $listuser;
		$data['listemail'] = $listemail;
		return $data;
	}
	
	private function processString($str)
	{
		$pos = strpos($str,'&lt;');
		if ($pos === false) 
		{
			return trim($str);
		}
		else
		{
			$s = $this->string->getSubString($str,'&lt;','&gt;');
			$s = str_replace('&','',$s);
			return trim($s);
		}
	}
	
	public function getuser_saleoffs($where, $from=0, $to=0)
	{
		
		$sql = "Select `user_saleoffsend`.*
									from `user_saleoffsend` 
									where 1=1 " . $where ;
		if($to > 0)
		{
			$sql .= " Limit ".$from.",".$to;
		}
		
		$query = $this->db->query($sql);
		return $query->rows;
	}
	
	public function updateStatus($id,$status)
	{
		$id=$this->db->escape(@$id);
		$status=$this->db->escape(@$status);
		$field=array(
						'id',
						'status',
					);
		$value=array(
						$id,
						$status
					);
		$where="id = '".$id."'";
		$this->db->updateData('user_saleoffsend',$field,$value,$where);
	}
	
	public function updateFolder($id,$folder)
	{
		$id=$this->db->escape(@$id);
		$folder=$this->db->escape(@$folder);
		$field=array(
						'id',
						'folder',
					);
		$value=array(
						$id,
						$folder
					);
		$where="id = '".$id."'";
		$this->db->updateData('user_saleoffsend',$field,$value,$where);
	}
	
	public function delectuser_saleoff($id)
	{
		$where="id = '".$id."'";
		$this->db->deleteData('user_saleoffsend',$where);
	}
}