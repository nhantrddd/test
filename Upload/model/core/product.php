<?php
$this->load->model("core/file");
class ModelCoreProduct extends ModelCoreFile 
{ 
	public function getItem($id)
	{
		$query = $this->db->query("Select `product`.* 
									from `product` 
									where id ='".$id."' ");
		return $query->row;
	}
	
	public function getList($where="", $from=0, $to=0)
	{
		
		$sql = "Select `product`.* 
									from `product` 
									where 1=1 " . $where;
									
		
		$order = " ORDER BY `position` ASC ,id DESC ";
		$sql .= $order;
		if($to > 0)
		{
			$sql .= " Limit ".$from.",".$to;
		}
				
		$query = $this->db->query($sql);
		return $query->rows;
	}
	
	public function getListStyles($groupkeys="",$sitemapid="", $from=0, $to=0)
	{
		$where ="";
		
		//refersitemap
		if(is_array($sitemapid) && count($sitemapid))
		{
			foreach($sitemapid as $item)
			{
				$arr[] = " refersitemap like '%[".$item."]%'";
			}
			
			$where .= "AND (". implode($arr," OR ").")";
		}
		elseif($sitemapid != "")
		{
			$where .= " AND refersitemap like '%[".$sitemapid."]%'";
		}
		
		//groupkeys
		if(is_array($groupkeys) && count($groupkeys))
		{
			foreach($groupkeys as $item)
			{
				$arr[] = " groupkeys like '%[".$item."]%'";
			}
			
			$where .= "AND (". implode($arr," OR ").")";
		}
		elseif($groupkeys != "")
		{
			$where .= " AND groupkeys like '%[".$groupkeys."]%'";
		}
		
		return $this->getList($where,$from, $to);
	}
	
	public function getByAlias($alias, $where="")
	{
		$query = $this->db->query("Select `product`.* 
									from `product` 
									where alias ='".$alias."' ".$where);
		return $query->row;
	}
	
	
	//sale off
	public function getListByParent($parent,$order = "", $from=0, $length=0)
	{
		$where = "AND productparent = '".$parent."' ".$order;		
		return $this->getProducts($where, $from, $length);		
		
		
	}
	
	public function getProducts($where="", $from=0, $to=5)
	{
		
		$sql = "Select `product`.* 
									from `product` 
									where status not like 'delete' " . $where ;
		if($to > 0)
		{
			$sql .= " Limit ".$from.",".$to;
		}
		
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function updateCol($id,$col,$val)
	{
		$id = $id;
		$col = $col;
		$val = $val;
		
		
		$field=array(
						$col
					);
		$value=array(
						$val
					);
		
		$where=" id = '".$id."'";
		$this->db->updateData('product',$field,$value,$where);
	}
}
?>