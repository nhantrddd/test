<?php
$this->load->model("core/file");
class ModelProductProduct extends ModelCoreFile 
{ 
	private $columns = array(	
								'productname',
								'productparent',
								'mediatype',
								'manufacturerid',
								'manufacturername',
								'imageid',
								'imagepath',
								'countryid',
								'materialid',
								'materialname',
								'color',
								'priceold',
								'price',
								'salenumber',
								'accessories',
								'refersitemap',
								'subimageid',
								'alias',
								'groupkeys',
								'description',
								'summary',
								'post',
								'warranty',
								'sale',
								'userid',
								'updatedate',
								'zonefrom',
								'zoneto',
								'startdate',
								'tourtype'
							);
							
	public function getItem($id, $where="")
	{
		$query = $this->db->query("Select `product`.* 
									from `product` 
									where id ='".$id."' ".$where);
		return $query->row;
	}
	
	public function getAlias($alias, $where="")
	{
		$query = $this->db->query("Select `product`.* 
									from `product` 
									where alias ='".$alias."' ".$where);
		return $query->row;
	}
	
	public function getList($where="", $from=0, $to=0)
	{
		
		$sql = "Select `product`.* 
									from `product` 
									where 1=1 " . $where ." ORDER BY `position` ASC ";
		if($to > 0)
		{
			$sql .= " Limit ".$from.",".$to;
		}
		
		$query = $this->db->query($sql);
		return $query->rows;
	}
	
	public function insert($data)
	{	
		foreach($this->columns as $val)
		{
			//if($val!="")
			{
				$field[] = $val;
				$value[] = $this->db->escape($data[$val]);	
			}
		}	
		$getLastId = $this->db->insertData("product",$field,$value);
		$this->updateFileTemp($data['imageid']);
		if($data['subimageid']!="")
		{
			$lisfileid=split(",",$data['subimageid']);
			foreach($lisfileid as $fileid)
			{
				$this->updateFileTemp($fileid);
			}
		}
		$this->updateCol($getLastId,'position',1);
		//
		$where = " AND refersitemap like '".$data['refersitemap']."' AND id <> '".$getLastId."'";
		$data_product = $this->getList($where,0,0);
		foreach($data_product as $key => $product)
		{
			$position = $key + 2;
			$this->updateCol($product['id'],'position',$position);
		}
		
		return $getLastId;
	}
	
	public function update($data)
	{	
		$id = @$this->db->escape($data['id']);
		foreach($this->columns as $val)
		{
			//if($val!="")
			{
				$field[] = $val;
				$value[] = $this->db->escape($data[$val]);	
			}
		}
		
		$where = " id = '".$id."'";
		$this->db->updateData('product',$field,$value,$where);
		$this->updateFileTemp($data['imageid']);
		
		if($data['subimageid']!="")
		{
			$lisfileid=split(",",$data['subimageid']);
			foreach($lisfileid as $fileid)
			{
				$this->updateFileTemp($fileid);
			}
		}
		return true;
	}
	public function updateCol($id,$col,$val)
	{
		$id = $id;
		$col = $col;
		$val = $val;
		
		
		$field=array(
						$col
					);
		$value=array(
						$val
					);
		
		$where=" id = '".$id."'";
		$this->db->updateData('product',$field,$value,$where);
	}
	public function delete($id)
	{
		$id =  $this->db->escape(@$id);
		$where="id = '".$id."'";
		$this->db->deleteData('product',$where);
	}
	
	/***** Begin Image ******/
	public function saveimagetemp($file, $filepath)
	{
		if($file['name'] != "")
		{
			$image = $this->model_core_file->saveFile($file,$filepath,"image","temp");
			if($image['fileid'] == '')
			{
				return "";
			}
			else
			{
				return $image["filepath"];	
			}
		}
		else
		{
			return "";
		}
	}
	
	public function saveimages($images, $productid)
	{
		$productid=$this->db->escape(@$productid);
		
		foreach($images as $image)
		{
			if($image['id'] == "")
			{
				$field = array(
						'productid',
						'imagepath'
					);
				$value=array(
								$productid,
								$image['imagepath']
							);
				$this->db->insertData("product_image",$field,$value);
			}
			else
			{
				if($image['status'] == "delete")
				{
					$where="id = '".$image['id']."'";
					$this->db->deleteData('product_image',$where);
				}	
			}
		}	
		
		return true;
	}
	
	public function getimages($productid)
	{
		$productid =  $this->db->escape(@$productid);
		$sql = "Select * from product_image where productid = '".$productid."'";
		$query = $this->db->query($sql);
		return $query->rows;
	}
	
	public function getInformation($id, $fieldname)
	{
		$sql = "Select * from product_information where productid = '".$id."' and fieldname = '".$fieldname."'";
		$query = $this->db->query($sql);
		$info = $query->row;
		return $info['fieldvalue'];
	}
	
	public function saveAttachment($id,$listfile)
	{
		if(count($listfile))
		{
			$listfileid=implode(",",$listfile);
			$this->saveInformation($mediaid, "attachment", $listfileid);
			$this->updateListFileTemp($listfile);
		}
		else
			$this->saveInformation($mediaid, "attachment", "");
	}
	
	public function clearTempFile()
	{
		$this->clearTemp();
	}
	
	public function saveInformation($id, $fieldname, $fieldvalue)
	{
		$sql = "Select * from product_information where productid = '".$id."' and fieldname = '".$fieldname."'";
		$query = $this->db->query($sql);
		$info = $query->rows;
		
		$field=array(
					"productid",
					"fieldname",
					"fieldvalue"
				);
				
		$value=array(
					$productid,
					$fieldname,
					$fieldvalue,
					);
	
		if(count($info) > 0)
		{
			$where="productid = '".$id."' AND fieldname = '".$fieldname."'";
			$this->db->updateData('product_information',$field,$value,$where);
		}
		else
		{
			$this->db->insertData("product_information",$field,$value);	
		}
	}
	/***** End Image ******/
	
	//sale off
	public function getListByParent($parent,$order = "", $from=0, $length=0)
	{
		$where = "AND productparent = '".$parent."' ".$order;		
		return $this->getProducts($where, $from, $length);		
		
		
	}
	
	public function getProducts($where="", $from=0, $to=5)
	{
		
		$sql = "Select `product`.* 
									from `product` 
									where status not like 'delete' " . $where ;
		if($to > 0)
		{
			$sql .= " Limit ".$from.",".$to;
		}
		
		$query = $this->db->query($sql);
		return $query->rows;
	}
	
	public function deleteProduct($id)
	{
		$sql = "Delete `product`.* 
									from `product`
									where id='".$id."'";
		$query = $this->db->query($sql);		
	}
	
	public function updatePosChild($data)
	{
		$id = $this->db->escape(@$data['id']);
		$position=(int)@$data['position'];
		
		
		$field=array(
						'position'
					);
		$value=array(
						$position
					);
		
		$where="id = '".$id."'";
		$this->db->updateData('product',$field,$value,$where);
	}
}
?>