<script type="text/javascript">
<!--
function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='index.php?lang="+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
//-->
</script>

<div id="ben-header">
	<div id="ben-top">
    	<div class="ben-left" id="ben-logo"> 
        	<div class="thanh-effect-logo">
            	<a href="<?php echo HTTP_SERVER_HOME ?>">		
                	<?php echo $imagepreview ?>
                </a> 
            </div>
       </div>
       <div class="ben-right myprofile">
           <?php 
           	if($this->user->isLogged()){
           		if($username != "") { 
           ?>
                Welcom: <b><a href="?route=common/profile"><?php echo $username ?></a></b>, 
                <a href="<?php echo HTTP_SERVER ?>logout.php">Logout</a>
           <?php 
           		}
           	} 
           ?>	
       </div>
       <div class="ben-right" id="ben-logo"> 
       		<a href="http://bensolution.com/">
            	<img src="<?php echo HTTP_SERVER.DIR_IMAGE ?>ben-logo_02.png" alt="Ben Solutions" height="86" /> 
            </a>
       </div>
      <div class="clearer">&nbsp;</div>
    </div>
    
    <div class="ben-menu">
        <div class="ben-wraper">
        <?php
            if($this->user->getUserTypeId() == 'admin'){
        ?>
            <div class="ben-navigation">
                <ul id="ben-main-nav">
                  <li><a class='<?php echo in_array("dashboard", $route)?"current-tab":"" ?>' 
                  		href='?route=common/dashboard'>Dashboard</a></li>
                  <li><a class='<?php echo in_array("sitemap", $route)?"current-tab":"" ?>' 
                  		href='?route=addon/sitemap'>Cấu trúc website</a></li>
                  <li><a class='<?php echo in_array("ext", $route)?"current-tab":"" ?>' 
                  		href='#'>Chức năng bổ sung</a>                        
                  		<ul>
                          <?php echo $addon ?>
                        </ul>
                  </li>
                  <li><a class='<?php echo in_array("fcn", $route)?"current-tab":"" ?>' 
                  		href='#'>Chức năng quản lý</a>
                  		<ul>
                        <?php 
                        	if(count($manag)){
                        		foreach($manag as $key=>$val){
                        ?>
                        	<li><a href="?route=<?php echo $key ?>"><?php echo $val ?></a></li>
            
                        <?php 
                        		} 
                        	}
                        ?>
                        </ul>
                  </li>
                  <li><a class='<?php echo in_array("media", $route)?"current-tab":"" ?>' 
                  		href='?route=core/media'>Quản lý thông tin</a></li>
                  <li><a class='<?php echo in_array("order", $route)?"current-tab":"" ?>' 
                  		href='?route=addon/order'>Quản lý bán hàng</a></li>
                  <li><a class='<?php echo in_array("acc", $route)?"current-tab":"" ?>' 
                  		>Tài khoản</a>
                  		<ul>
                        <?php 
                        	if(count($account)){
                        		foreach($account as $key=>$val){
                        ?>
                        	<li><a href="?route=<?php echo $key ?>"><?php echo $val ?></a></li>
            
                        <?php 
                        		} 
                        	}
                        ?>
                        	<li><a href="<?php echo HTTP_SERVER ?>logout.php">Logout</a></li>
                        </ul>
                  </li>
                </ul>
            	<div class="clearer">&nbsp;</div>
          	</div>
      	<?php
        	}
        ?>
        </div>
    </div>
    
</div>