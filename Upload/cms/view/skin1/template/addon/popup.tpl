<div class="section" id="sitemaplist">
    <div class="section-title"><?php echo $text_popup_header; ?></div>
    <div class="section-content padding1">
        <form name="frm" id="frm" action="?route=addon/popup/save" method="post" enctype="multipart/form-data">
            <div class="button right">
                <input type="submit" value="<?php echo $button_save ?>" class="button" />
                <input type="button" value="<?php echo $button_cancel ?>" class="button" onclick="linkto('?route=common/dashboard')"/>   
                <input type="hidden" name="id" value="<?php echo $item['id']; ?>">
            </div>
            <div class="clearer">^&nbsp;</div>
            <div id="error" class="error" style="display:none;"></div>
            <div>   
                <p>
                    <label><?php echo $text_popup_title; ?></label><br />
                    <input type="text" name="title" value="<?php echo $item['title']; ?>" class="text" size=60 <?php if($item['categoryid']!="") echo 'readonly="readonly"'?>/>
                </p>     
                <p>
                    <label><?php echo $text_popup_width; ?></label><br />
                    <input type="text" name="width" value="<?php echo $item['width']; ?>" class="text" size=60 />
                </p>
                <p>
                    <label><?php echo $text_popup_height; ?></label><br />
                    <input type="text" name="height" value="<?php echo $item['height']; ?>" class="text" size=60 />
                </p>
                <p>
                    <label><?php echo $text_popup_number; ?></label><br />
                    <input type="text" name="number" value="<?php echo $item['number']; ?>" class="text" size=60 />
                </p>
                <p>
                    <label><?php echo $text_popup_status; ?></label><br />
                    <select id="status" name="status" class="text">
                        <option value="1" <?php echo ($item['status'] == 1) ? 'selected="selected"' : ''; ?>><?php echo $text_popup_active; ?></option>
                        <option value="0" <?php echo ($item['status'] == 0) ? 'selected="selected"' : ''; ?>><?php echo $text_popup_inactive; ?></option>
                    </select>
                </p>
                <p>
                    <label><?php echo $text_popup_detail; ?></label><br />
                    <textarea class="text" rows="3" cols="70" id="detail" name="detail"><?php echo $item['detail']; ?></textarea>
                    <script language="javascript">
                    $(document).ready(function(e) {
                        setCKEditorType('detail', 2);
                    });
                    </script>
                </p>
            </div>
        </form>
    </div>
</div>