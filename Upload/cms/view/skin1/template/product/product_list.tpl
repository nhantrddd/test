<script src='<?php echo DIR_JS?>ui.datepicker.js' type='text/javascript' language='javascript'> </script>
<div class="section">

    <div class="section-title">Quản lý Sản phẩm</div>
        
    <div class="section-content">
        
        <form action="" method="post" id="listitem" name="listitem">
            <div id="ben-search">
            	<label>Tên sản phẩm</label>
                <input type="text" id="productname" name="productname" class="text" value="" />
                <!--<label>Hảng sản xuất</label>
                <select id="manufacturer">
                    <option value="">- Tất cả sản phẩm -</option>
                    <?php echo $cbmanufacturer?>
                </select>-->
                <label>Giá từ</label>
                <select id="giatu" class="text">
                    <option value="">- Nhập số tiền - </option>
                    <?php foreach($sotien as $val){ ?>
                    <option value="<?php echo $val?>"><?php echo $this->string->numberFormate($val)?> <?php echo $this->document->setup['Currency']?></option>
                    <?php } ?>
                </select>
                <label>Đến</label>
                <select id="giaden" class="text">
                    <option value="">- Nhập số tiền - </option>
                    <?php foreach($sotien as $val){ ?>
                    <option value="<?php echo $val?>"><?php echo $this->string->numberFormate($val)?> <?php echo $this->document->setup['Currency']?></option>
                    <?php } ?>
                </select>
               
                <input type="button" class="button" name="btnSearch" value="Tìm" onclick="searchForm()"/>
                <input type="button" class="button" name="btnSearch" value="Xem tất cả" onclick="window.location = '?route=module/product&sitemapid=<?php echo $_GET['sitemapid']?>'"/>
            </div>
            <div class="button right">
            <?php if($dialog != "true"){ ?>
            	
                <input type="button" class="button" value="Cập nhật vị trí"  onclick="updatePos()"/>
           	 	<input class="button" type="button" name="add" value="Thêm" onclick="linkto('<?php echo $insert ?>')"/>
                <input class="button" type="button" name="delete_all" value="Xóa" onclick="deleteItem()"/>  
            <?php } else { ?>
            	<input class="button" type="button" name="select" value="Chọn" onclick="selectItem()"/>
            <?php } ?>
            </div>
            <div class="clearer">^&nbsp;</div>
            
            <div class="sitemap treeindex">
            
            	<table class="data-table" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr class="tr-head">
                        <th width="1%"><input class="inputchk" type="checkbox" onclick="$('input[name*=\'delete\']').attr('checked', this.checked);"></th>
                        <th>STT</th>
                        <th>Tên sản phẩm</th>
                        <th>Hình ảnh</th>
                        <!--<th>Nhà sản xuất</th>
                        <th>Xuất xứ</th>       
                        <th>Chất liệu</th>-->
                        <th>Giá</th>
                       	<?php if($dialog != "true"){?>                                  
                        <!--<th>Màu sắc</th>-->
                        <th>Mô tả</th>
                        <!--<th>Sản phẩm phụ trợ</th>-->
                        <th>Công cụ</th>
                        <?php } ?>
                    </tr>
        
        <?php
        if(count($datas) > 0)
        {
        	$stt = 1;
            foreach($datas as $item)
            {
        ?>
                    <tr>
                        <td class="check-column"><input class="inputchk" type="checkbox" name="delete[<?php echo $item['id']?>]" value="<?php echo $item['id']?>" ></td>
                        
                        <td><input type="text" class="text sort" name="position[<?php echo $item['id']?>]" size="1" value="<?php echo $item['position']?>"/></td>
                        <td><?php echo $item['productname']?></td>
                        <?php if($item['imagepath'] != ""){ ?>
                        <td><?php echo $item['imagepreview']?></td>
                        <?php }else { ?>
                        <td></td>
                        <?php } ?>
                        <!--<td><?php echo $item['manufacturername']?></td>
                        <td><?php echo $item['countryname']?></td>
                        <td><?php echo $item['materialname']?></td>-->
                        <td><?php echo $this->string->numberFormate($item['price'])?></td>
                        <?php if($dialog != "true"){ ?>
                        <!--<td><?php echo $item['color']?></td>-->
                        <td><?php echo $this->string->getNumberTextLength(html_entity_decode($item['summary']) , 0, 30)?></td>
                        <!--<td>
                        <?php
                        	if(count($item['accessories']) > 0) {
                            	foreach($item['accessories'] as $accessory){
                                if(count($accessory)>0)
                                {
                        ?>
                        	<ul>
                            	<li><?php echo $accessory['productname'] ?></li>
                            </ul>
                        <?php
                        		} 
                        	} 
                        }                        
                        ?>
                        </td>-->
                        <td><input class="button" type="button" value="Sửa" onclick="linkto('<?php echo $item[link_edit] ?>')"  /></td>
                        <?php } ?>
                    </tr>
        <?php
            }
        }
        ?>                                  
                </tbody>
                </table>
            </div>
            <?php echo $pager?>
        
        </form>
        
    </div>
    
</div>
<script language="javascript">

function deleteItem()
{
	var answer = confirm("Bạn có muốn xóa không?")
	if (answer)
	{
		$.post("?route=product/product/delete", 
				$("#listitem").serialize(), 
				function(data)
				{
					if(data == "true")
					{
						alert('Xóa thành công');
						window.location.reload();
					}
				}
		);
	}
}

function selectItem()
{
	var str =""
	
	$('.inputchk').each(function(){
		
		if(this.checked == true)
		{
			str += this.value + ",";
		}
	});
	
	//alert(opener.document.frm.selectproduct.value = str);
	opener.document.frm.selectproduct.value = str;
	window.close();
}
function updatePos()
{
	$.blockUI({ message: "<h1>Đang xử lý...</h1>" }); 
	$.post("?route=product/updatePos",$('#listitem').serialize(),function(data){
		if(data=="true")
		{
			alert('Cập nhật thành công');	
			window.location.reload()
		}
	});	
}
function searchForm()
{
	var url = "?route=module/product&sitemapid=<?php echo $_GET['sitemapid']?>";
	if($('#productname').val()!="")
	{
		url += "&productname="+$('#productname').val();
	}
	if($('#manufacturer').val()!="")
	{
		url += "&manufacturer="+$('#manufacturer').val();
	}
	
	if($('#giatu').val()!="")
	{
		url += "&giatu="+$('#giatu').val();
	}
	if($('#giaden').val()!="")
	{
		url += "&giaden="+$('#giaden').val();
	}
	window.location = url;
}
$('#productname').val("<?php echo urldecode($_GET['productname'])?>");
$('#manufacturer').val("<?php echo $_GET['manufacturer']?>");
$('#sitemapid').val("<?php echo $_GET['sitemapid']?>");
$('#giatu').val("<?php echo $_GET['giatu']?>");
$('#giaden').val("<?php echo $_GET['giaden']?>");
</script>