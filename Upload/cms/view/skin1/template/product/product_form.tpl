<?php
	/*echo "<pre>";
    print_r($zonesto);
    echo "</pre>";
    break;*/
?>
<div class="section" id="sitemaplist">

	<div class="section-title">Quản lý sản phẩm</div>
    
    <div class="section-content padding1">
    
    	<form name="frm" id="frm" action="<?php echo $action?>" method="post" enctype="multipart/form-data">
        
        	<div class="button right">
            	<input type="button" value="Lưu" class="button" onClick="save()"/>
     	        <input type="button" value="Bỏ qua" class="button" onclick="goback()"/>
     	        <input type="hidden" id="mediaid" name="id" value="<?php echo $item['id']?>">
                
                <input type="hidden" id="handler" />
             	<input type="hidden" id="outputtype" />
            </div>
            <div class="clearer">&nbsp;</div>
            
		<div id="container">           
            <ul>
            	<li class="tabs-selected"><a href="#fragment-content"><span>Biên tập thông tin</span></a></li>
                <li><a href="#fragment-summary"><span>Thông tin vắn tắt</span></a></li>
                <li><a href="#fragment-description"><span>Thông tin chi tiết</span></a></li>
                <li><a href="#fragment-post"><span>Bài viết</span></a></li>
                <!--<li><a href="#fragment-warranty"><span>Bảo hành</span></a></li>
                <li><a href="#fragment-sale"><span>Khuyến mãi</span></a></li>
                <li><a href="#fragment-categories"><span>Sản phẩm phụ kiện</span></a></li>-->
                <li><a href="#fragment-map"><span>Danh mục sản phẩm</span></a></li>
            </ul>
            
            <div id="fragment-content">
            <div id="error" class="error" style="display:none"></div>
        	<div>
            	<div class="col3 left">
                	<p>
                        <label for="image">Hình ảnh đại diện</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a class="button btnAddImage">Chọn hình ảnh</a><br /><br />
                        <img id="imagepreview" class="btnAddImage2" src="" />
                        <input type="hidden" id="imagepath" name="imagepath" value="<?php echo $item['imagepath']?>" />
                        <input type="hidden" id="imageid" name="imageid" value="<?php echo $item['imageid']?>"  />
                        <input type="hidden" id="imagethumbnail" name="imagethumbnail" />
                        <!--<?php echo $item['imagepreview'] ?>-->
                    </p>
                    <p>
                        <label for="image">Hình ảnh phụ</label>
                        <a id="btnAddAttachment" class="button">Chọn hình ảnh</a><br />
                        <p id="attachment">
                        </p>
                        <!--<img id="subimage_preview" src="" />
                        <input type="hidden" id="sub_imagepath" name="sub_imagepath" value="<?php echo $item['subimage']?>" />
                        <input type="hidden" id="sub_imagethumbnail" name="sub_imagethumbnail" />-->
                        <!--<?php echo $item['imagepreview'] ?>-->
                       
                    	
                        <span id="delfile"></span>
                        
                        <script language="javascript">
							$(document).ready(function() {
							// put all your jQuery goodness in here.
						<?php
								foreach($attachment as $value)
								{
									if(count($value))
									{
						?>
									$('#attachment').append(creatAttachmentRow("<?php echo $value['fileid']?>","<?php echo $value['filename']?>","<?php echo $value['subimg']?>"));
						<?php
									}
								}
						?>
							});
						
						</script>
                        
                        
                    </p>
                </div>
            	<div class="col6 left">
                    <p>
                        <label>Tên sản phẩm:</label><br />
                        <input type="text" id="title" name="productname" value="<?php echo $item['productname']?>" class="text"  style="width:290px" />
                    </p>
                    <p>
                    	<label>Alias:</label><br />
                        <input class="text" type="text" id="alias" name="alias" value="<?php echo $item['alias'] ?>" style="width:290px;" />
                    </p>
<script>
$('#title').change(function(e) {
    $.ajax({
			url: "?route=common/api/getAlias&title=" + toBasicText(this.value),
			cache: false,
			success: function(html)
			{
				$("#alias").val(html);
			}
	});
});
</script>         

                    <p>
                    
                    	<script language="javascript">
						$(function() {
							$("#startdate").datepicker({
									changeMonth: true,
									changeYear: true,
									dateFormat: 'dd-mm-yy',
									
									});
							});
						</script>
                        <label>Ngày khởi hành:</label><br />
						<input type="text" class="text" id="startdate" name="startdate" 
                        	value="<?php echo $this->date->formatMySQLDate($item['startdate']) ?>" />
                    </p>
                    <p>
                    	<label>Loại tour:&nbsp;</label>
                        <input type="radio" id="tourTrongNuoc" name="tourtype" value="trongnuoc" 
                        	 onchange="selectzoneto('trongnuoc')"
                        	<?php echo ($item['tourtype'] == '' || $item['tourtype'] == 'trongnuoc')?"checked='checked'":""; ?> />
                        <span>Trong nước</span>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="radio" id="tourNuocNgoai" name="tourtype" value="nuocngoai" 
                        	 onchange="selectzoneto('nuocngoai')"
                        	<?php echo ($item['tourtype'] == 'nuocngoai')?"checked='checked'":""; ?> />
                        <span>Nước ngoài</span>
                    </p>
                    <p>
                        <label>Nơi khởi hành:</label><br />
                        <select name="zonefrom" id="zonefrom" class="text">
                        	<?php
                                if(count($zones)){
                                    foreach($zones as $zone){
                            ?>
                            <option value="<?php echo $zone['zoneid'] ?>" <?php echo ($item['zonefrom']==$zone['zoneid'])?'selected="selected"':''; ?> >
                                <?php echo $zone['zonename'] ?>
                            </option>
                            <?php
                                    }
                                }
                            ?>
                        </select>
                    </p>
                    <p>
                        <label>Điểm đến:</label><br />
                        <select name="zoneto" id="zoneto" class="text">
                        	<?php echo $zonesto ?>
                            <!--<?php
                                if(count($zonesto)){
                                    foreach($zonesto as $zone){
                            ?>
                            <option value="<?php echo $zone['zoneid'] ?>" <?php echo ($item['zoneto']==$zone['zoneid'])?'selected="selected"':''; ?> >
                                <?php echo $zone['zonename'] ?>
                            </option>
                            <?php
                                    }
                                }
                            ?>-->
                        </select>
                    </p>
                    <!--   
					<p>
                        <label>Nhà sản xuất:</label><br />
                        <select id="manufacturerid" name="manufacturerid" style="width:300px">
                            <?php echo $manufacturer ?>
                        </select>
                    </p>
                    <p>
                        <label>Xuất xứ:</label><br />
                        <select name="countryid" id="countryid" style="width:300px">
                            <?php echo $country ?>
                        </select>
                    </p>
                    <p>
                        <label>Chất liệu:</label><br />
                       <select id="materialid" name="materialid" style="width:300px">
                            <?php echo $material ?>
                       </select>
                    </p>-->
                    
                    <!--<p>
                        <label>Màu sắc:</label><br />
                        <input type="text" name="color" value="<?php echo $item['color']?>" class="text" size=60 />
                    </p>-->
                    <p>
                        <label>Giá cũ:</label><br />
                        <input type="text" name="priceold" value="<?php echo $item['priceold']?>" class="text number"  style="width:290px" />
                    </p>
                    <p>
                        <label>Giá gốc:</label><br />
                        <input type="text" name="price" value="<?php echo $item['price']?>" class="text number"  style="width:290px" />
                    </p>
                   <!-- <p>
                    	<input type="checkbox" id="saleoff" name="saleoff" value="" class="" /> SALE OFF
                    </p>-->
                    <p>
                        <label>Trạng thái:</label><br />
                        <?php foreach($statuspro as $it){ ?>
                           <div>
                            <?php echo $this->string->getPrefix("&nbsp;&nbsp;&nbsp;&nbsp;",$it['level']) ?>
                                <input type="checkbox"  name="loaisp[<?php echo $it['categoryid']?>]" value="<?php echo $it['categoryid']?>" <?php echo in_array($it['categoryid'],$properties)?'checked="checked"':''; ?> />
                            <?php echo $it['categoryname']?>
                           </div>
                        <?php } ?>
                    </p>
                    
                    <!--<table>
                    	<tr>
                        	<td width="30%">
                            	<input type="hidden" name="price_mediaid" id="price_mediaid" />
                            	<p>
                                    <label>Số lượng khuyến mãi:</label><br />
                                    <input class="text number" type="text" name="number_total" id="number_total" value="" size="20" />
                                </p>
                            </td>
                            <td width="30%">
                            	<p>
                                    <label>Giá khuyến mãi:</label><br />
                                    <input class="text number" type="text" name="price_khuyenmai" id="price_khuyenmai" value="" size="20" />
                                </p>
                            </td>
                            <td width="30">
                            	 <p>
                                    Sale Price:<br />
                                    <input class="text number" type="text" name="price_gia" id="price_gia" value="" size="20" />
                                </p>
                            </td>
                            <td width="50%">
                            	<input type="button" class="button" id="btnSavePrice" value="<?php echo $button_save?>"/>
                        		<input type="button" class="button" id="btnCancelPrice" value="<?php echo $button_cancel?>"/>
                            </td>
                        </tr>
                        
                    </table>-->
                
                </div>
                
            	<!--Load thông tin khuyến mãi-->
                <div id="pricelist" style="display:none">
                <div class="clearer">&nbsp;</div>
                 
                </div>
               	
               
            </div>
            </div>
            <div id="fragment-categories">
            <div>
				<p>
                    <label>Sản phẩm phụ kiện:</label>
                    <input type="button" value="Thêm" class="button" id="btnAdd"/>
                    <input type="button" value="Xóa dòng" class="button" id="btnDelRows" />
                    <input type="hidden" id="selectproduct" name="selectproduct">
                    <table>
                    	<thead>
                        	<th width="1%"><input class="inputchk" type="checkbox" onclick="$('.inputchk').attr('checked', this.checked);"></th>
                            <th>Tên sản phẩm</th>
                            <th>Hình ảnh</th>
                            <th>Nhà sản xuất</th>
                            <th>Xuất xứ</th>
                            <th>Chất liệu</th>
                            <th>Giá</th>
                        </thead>
                        <tbody id="listproduct">
                        </tbody>
                    </table>
                </p>
            </div>
            </div>
            <div id="fragment-post">
                <p>
                	<label>Bài viết:</label>
                    <a class="button" onclick="browserFileImage4()"><?php echo $entry_photo ?></a>
                	<input type="hidden" id="listselectfile" name="listselectfile" />
                    <textarea name="post" id="editor5" cols="80" rows="10"><?php echo $item['post']?></textarea>
                </p>
            </div>
            <div id="fragment-description">
                <p>
                	<label>Chi tiết:</label>
                    <a class="button" onclick="browserFileImage()"><?php echo $entry_photo ?></a>
                	<input type="hidden" id="listselectfile" name="listselectfile" />
                    <textarea name="description" id="editor1" cols="80" rows="10"><?php echo $item['description']?></textarea>
                </p>
            </div>
            <div id="fragment-warranty">
            	<p>
                	<label>Bảo hành:</label>
                    <a class="button" onclick="browserFileImage3()"><?php echo $entry_photo ?></a>
                	<input type="hidden" id="listselectfile" name="listselectfile" />
                    <textarea name="warranty" id="editor4" cols="80" rows="10"><?php echo $item['warranty']?></textarea>
                </p>
            </div>
            <div id="fragment-sale">
            	<p>
                	<label>Khuyến mãi:</label>
                    <a class="button" onclick="browserFileImage2()"><?php echo $entry_photo ?></a>
                	<input type="hidden" id="listselectfile" name="listselectfile" />
                    <textarea name="sale" id="editor3" cols="80" rows="10"><?php echo $item['sale']?></textarea>
                </p>
                
            </div>
            <div id="fragment-summary">
            <div>
                <p>
                    <label>Thông tin vắn tắt:</label>
                    <a class="button" onclick="browserFileImage1()"><?php echo $entry_photo ?></a>
                	<input type="hidden" id="listselectfile" name="listselectfile" />
                    <br />
                    <textarea name="summary" id="editor2" cols="80" rows="10"><?php echo $item['summary']?></textarea>
                </p>
            </div>   
            
            </div>
            
            <div id="fragment-map">
                <div>
                	<table>
                    	<thead>
                        	<th width="50%"><?php echo $column_menu?></th>
                            <th width="50%"><?php echo $column_parent?></th>
                        </thead>
                        <tbody>
                        	<?php echo $listReferSiteMap?>
                        </tbody>
                    </table> 
                </div>
            </div>
        </div>   
        </form>
    
    </div>
    
</div>

<script src='<?php echo DIR_JS?>ajaxupload.js' type='text/javascript' language='javascript'> </script>
<script language="javascript">
var DIR_UPLOADPHOTO = "<?php echo $DIR_UPLOADPHOTO?>";
var DIR_UPLOADATTACHMENT = "<?php echo $DIR_UPLOADATTACHMENT?>";

function selectzoneto(tourtype){
	//$("#zoneto").load("?route=product/product/loadPrice&mediaid="+$("#mediaid").val());
	$.post("?route=product/product/selectzoneto&tourtype="+tourtype, 
		function(data) 
		{	
			$("#zoneto").html(data);			
			numberReady();
		});
}

$(document).ready(function() {
	
	$("#pricelist").load("?route=product/product/loadPrice&mediaid="+$("#mediaid").val());
	
	setCKEditorType('editor1',2);
	setCKEditorType('editor2',2);
	setCKEditorType('editor3',2);
	setCKEditorType('editor4',2);
	setCKEditorType('editor5',2);
	$('#container').tabs({ fxSlide: true, fxFade: true, fxSpeed: 'slow' });
	
	$('#imagepreview').attr("src", "<?php echo $item['imagepreview'] ?>");
	
	<?php
	if(count($item['accessories']) > 0)
	{
		foreach($item['accessories'] as $product){
			if(count($product)>0){
	?>
		product.addRow("<?php echo $product['id'] ?>");
	<?php
			}
		}
	}
	?>
	
	
});

/*function browserFileImage()
{
	$('#handler').val('image');
	$('#outputtype').val('image');
	showPopup("#popup", 800, 500);
	$("#popup").html("<img src='view/skin1/image/loadingimage.gif' />");
	$("#popup").load("?route=core/file")
		
}*/

$('#btnAdd').click(function(){
	var result = window.showModalDialog("index.php?route=module/product&dialog=true","", "dialogWidth:1000px; dialogHeight:800px; center:yes");
	
	var arr = $('#selectproduct').val().split(',');
	for(i=0;i<arr.length;i++)
	{
		if(arr[i]!="")
		{
			var flag = false;
			$('.accessoryid').each(function(){
				if(this.value == arr[i])
				{
					flag = true;	
				}
			});
			
			if(flag == false)
			{
				product.addRow(arr[i]);
			}
			else
			{
				alert('Bạn đã chọn sản phẩm này rồi');	
			}
		}
	}
});

$('#btnDelRows').click(function(){
	$('.rowitem').each(function(){
		
		if(this.checked == true)
		{
			$('#row'+this.value).remove();
		}
	});
	$('.inputchk').attr('checked', false);
});

function Product()
{
	this.index = 0;
	this.addRow = function(id)
	{
		$.getJSON("?route=product/product/getProduct&col=id&val="+id, 
		function(data) 
		{
			for(i in data.products)
			{
				//Du lieu post di
				var hidden_productid = '<input type="hidden" class="accessoryid" id="accessoryid-'+ product.index +'" name="accessoryid['+ product.index +']" value="'+data.products[i].id+'">'; 
				
				var cellchk = '<td><input type="checkbox" class="rowitem inputchk" value="'+ product.index +'"></td>';
				var cellproduct = '<td>'+hidden_productid+data.products[i].productname+'</td>';
				var cellimage = '<td><img src="'+ data.products[i].imagepreview+'"/></td>';
				var cellmanufacturer = '<td>'+data.products[i].manufacturername+'</td>';
				var cellcountryname = '<td>'+data.products[i].countryname+'</td>';
				var cellmaterialname = '<td>'+data.products[i].materialname+'</td>';
				var cellprice = '<td class="number">'+formateNumber( data.products[i].price)+'</td>';
				
				var row = '<tr id="row'+ product.index +'">'
				row += 	cellchk;
				row +=	cellproduct;
				row +=  cellimage;
				row +=	cellmanufacturer;
				row +=  cellcountryname;
				row +=	cellmaterialname;	
				row +=	cellprice;
				
				row += '</tr>';
				$('#listproduct').append(row);
				product.index++;
				numberReady();
			}
			
		});
	}
	
}
var product = new Product();


function browserFileImage()
{
    //var re = openDialog("?route=core/file&dialog=true",800,500);
	$('#handler').val('editor1');
	$('#outputtype').val('editor');
	showPopup("#popup", 800, 500);
	$("#popup").html("<img src='view/skin1/image/loadingimage.gif' />");
	$("#popup").load("?route=core/file&dialog=true");
		
}

function browserFileImage1()
{
	$('#handler').val('editor2');
	$('#outputtype').val('editor');
	showPopup("#popup", 800, 500);
	$("#popup").html("<img src='view/skin1/image/loadingimage.gif' />");
	$("#popup").load("?route=core/file&dialog=true");
		
}

function browserFileImage2()
{
	$('#handler').val('editor3');
	$('#outputtype').val('editor');
	showPopup("#popup", 800, 500);
	$("#popup").html("<img src='view/skin1/image/loadingimage.gif' />");
	$("#popup").load("?route=core/file&dialog=true");
		
}

function browserFileImage3()
{
	$('#handler').val('editor4');
	$('#outputtype').val('editor');
	showPopup("#popup", 800, 500);
	$("#popup").html("<img src='view/skin1/image/loadingimage.gif' />");
	$("#popup").load("?route=core/file&dialog=true");
		
}

function browserFileImage4()
{
	$('#handler').val('editor5');
	$('#outputtype').val('editor');
	showPopup("#popup", 800, 500);
	$("#popup").html("<img src='view/skin1/image/loadingimage.gif' />");
	$("#popup").load("?route=core/file&dialog=true");
		
}

function save()
{
	$.blockUI({ message: "<h1>Đang xử lý...</h1>" });  
	var oEditor = CKEDITOR.instances['editor1'] ;
	var pageValue = oEditor.getData();
	$('textarea#editor1').val(pageValue);
	var oEditor = CKEDITOR.instances['editor2'] ;
	var pageValue = oEditor.getData();
	$('textarea#editor2').val(pageValue);
	var oEditor = CKEDITOR.instances['editor3'] ;
	var pageValue = oEditor.getData();
	$('textarea#editor3').val(pageValue);
	var oEditor = CKEDITOR.instances['editor4'] ;
	var pageValue = oEditor.getData();
	$('textarea#editor4').val(pageValue);
	var oEditor = CKEDITOR.instances['editor5'] ;
	var pageValue = oEditor.getData();
	$('textarea#editor5').val(pageValue);
	$.post("?route=product/product/save", $("#frm").serialize(),
		function(data){
			if(data == "true")
			{
				goback();
			}
			else
			{
			
				$('#error').html(data).show('slow');
				$.unblockUI();
				
			}
			
		}
	);
}

//var thumb = $('#subimage_preview');
function goback()
{
	var re = "?<?php echo $_SESSION['return']?>";
	if(re == '')
		re = "?route=module/product&sitemapid=<?php echo $_GET['sitemapid'] ?>";
	window.location = re;
}

function addImageTo()
{
	var str= trim($("#listselectfile").val(),",");
	var arr = str.split(",");

	if(str!="")
	{
		for (i=0;i<arr.length;i++)
		{
			$.getJSON("?route=core/file/getFile&fileid="+arr[i], 
				function(data) 
				{
					switch($('#outputtype').val())
					{
						case 'editor':
							width = "";
							
							var value = "<img src='<?php echo HTTP_IMAGE?>"+data.file.filepath+"'/>";

							var oEditor = CKEDITOR.instances[$('#handler').val()] ;
							
							// Check the active editing mode.
							if (oEditor.mode == 'wysiwyg' )
							{
								// Insert the desired HTML.
								oEditor.insertHtml( value ) ;
								$("#listselectfile").val('');
								var temp = oEditor.getData()
								oEditor.setData( temp );
							}
							else
								alert( 'You must be on WYSIWYG mode!' ) ;
							break;
						case 'image':
							var handler = $('#handler').val();
							$('#'+handler+'id').val(data.file.fileid)
							$('#'+handler+'path').val(data.file.filepath)
							$.getJSON("?route=core/file/getFile&fileid="+data.file.fileid+"&width=200", 
							function(file) 
							{
								$('#'+handler+'thumbnail').val(file.file.imagepreview)
								$('#'+handler+'preview').attr('src',file.file.imagepreview)
							});
							break;
						case 'file':
							var handler = $('#handler').val();
							$('#'+handler+'id').val(data.file.fileid);
							$('#'+handler+'path').val(data.file.filepath);
							$('#'+handler+'name').html(data.file.filename);
							break;
						case 'attachment':
							var handler = $('#handler').val();
							$.getJSON("?route=core/file/getFile&fileid="+data.file.fileid+"&width=50", 
							function(file) 
							{
								$('#'+handler).append(attachment.creatAttachmentRow(data.file.fileid,data.file.filename,file.file.imagepreview));
								
							});
							
							break;
					}
				});
		}
	}
}



new AjaxUpload(jQuery('.btnAddImage'), {
	action: DIR_UPLOADPHOTO,
	name: 'image2',
	responseType: 'json',
	onChange: function(file, ext){
		//$('#sub_preview').hide();
	},
	onSubmit: function(file, ext){
		// Allow only images. You should add security check on the server-side.
		if (ext && /^(jpg|png|jpeg|gif)$/i.test(ext)) {                            
			$('#pnImage').hide();
			$('.loadingimage').show();
		} else {
			alert('Your selection is not image');
			return false;
		}                            
	},
	onComplete: function(file, response){
		if(response.files.error == 'none')
		{
			$('input#imageid').val(response.files.imageid);
			$('input#imagepath').val(response.files.imagepath);
			//$('input#sub_imagethumbnail').val(response.files.imagethumbnail);
			$('#imagepreview').attr("src", response.files.imagethumbnail);
			//alert('abc');
			$('#errorupload').hide();	
		}
		else
		{
			$('#errorupload').html(response.files.error);
			$('#errorupload').show();
		}
		$('#pnImage').show();
		$('.loadingimage').hide();
		
	}
});

</script>

<!--Giá khuyến mãi-->
<script type="text/javascript">
$("#btnSavePrice").click(function(){
	 var price = $("#price_khuyenmai").val().replace(/,/g,"");
		$.post("?route=product/product/save", 
					{
						id : $("#price_mediaid").val(), 
						productparent : $("#mediaid").val(),
						productname : $("#title").val(), 
						salenumber: $("#number_total").val(),
						mediatype : 'price',
						price : price
					},
			function(data){
				if(data=="true")
				{
					$("#pricelist").load("?route=product/product/loadPrice&mediaid="+$("#mediaid").val());
					$("#price_mediaid").val("");
					$("#number_total").val(0);
					$("#price_khuyenmai").val(0);
				}
				else
				{
					$("#subimageerror").html(data);
					$("#subimageerror").show('slow');
				}
				
			});
});

$("#btnCancelPrice").click(function(){
	$("#price_mediaid").val(""); 
	$("#number_total").val(0);
	$("#price_khuyenmai").val(0);			
});



var price = new Price();
function Price()
{
	/*this.loadPrice = function(code)
	{
		$.getJSON("<?php echo HTTP_SERVER?>ric/getSanPham.php?masanpham="+code, 
			function(data) 
			{
				if(data.sanpham == false)
					alert('Không tồn tại code sản phẩm này');
				else
					$('#price_gia').val(formateNumber(data.sanpham.HH_GiaBan+""));
				
				
			});
	}*/
	
	this.save = function()
	{
		var price = $("#price_khuyenmai").val().replace(/,/g,"");
		$.post("?route=product/product/save", 
					{
						id : $("#price_mediaid").val(), 
						productparent : $("#mediaid").val(),
						productname : $("#title").val(), 
						salenumber: $("#number_total").val(),
						mediatype : 'price',
						price : price
					},
			function(data){
				if(data=="true")
				{
					$("#pricelist").load("?route=product/product/loadPrice&mediaid="+$("#mediaid").val());
					$("#price_mediaid").val("");
					$("#number_total").val(0);
					$("#price_khuyenmai").val(0);
				}
				else
				{
					$("#subimageerror").html(data);
					$("#subimageerror").show('slow');
				}
				
			});
	}
	this.edit = function(id)
	{
		$.getJSON("?route=product/product/getPrice&mediaid="+id, 
			function(data) 
			{
				
				$("#price_mediaid").val(data.price.id);
				//$("#price_title").val(data.price.productname);
				$("#number_total").val(formateNumber(formateNumber(data.price.salenumber)));
				$("#price_khuyenmai").val(formateNumber(formateNumber(data.price.price)));
				//$("#price_khuyenmai").val(formateNumber(data.price.khuyenmai));
				
		
				numberReady();
				
			});
	}
	this.remove = function(id)
	{
		//$.blockUI({ message: "<h1>Please wait...</h1>" });
		$.ajax({
			url: "?route=product/product/deleteProduct&mediaid="+id, 
			cache: false,
			success: function(html)
			{
				$("#pricelist").load("?route=product/product/loadPrice&mediaid="+$("#mediaid").val());
			}
		});
	}
}

</script>
<script src='<?php echo DIR_JS?>uploadattament.js' type='text/javascript' language='javascript'> </script>
<script src="<?php echo DIR_JS?>jquery.tabs.pack.js" type="text/javascript"></script>
<script src='<?php echo DIR_JS?>ajaxupload.js' type='text/javascript' language='javascript'> </script>

