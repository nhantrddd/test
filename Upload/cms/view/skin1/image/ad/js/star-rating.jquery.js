/*
 * jQuery star rating plugins
 * 
 * @author: Pham Van An
 * @version: 1.0
 */
( function($) 
{
	$.fn.starRating = function(options) 
	{
		var settings = 
		{
			stars_number: 10,
			stars_total: 0,
			stars_count: 0,
			vote_class: 'star_vote',
			novote_class: 'star_novote',
			hover_class: 'star_hover',
			onRate: function(value)
			{
				alert('you voted '+ value);
			}
		};
		
		if ( options )
		{ 
			$.extend( settings, options );
		}
		
		// default value is no star voted
		var no_vote = 0;
		var float_part = 0.0;
		var division_number = settings.stars_total*1.0/settings.stars_count;
		
		// calculate the vote|novote stars
		if(settings.stars_total > 0 && division_number  < settings.stars_number)
		{
			no_vote = Math.floor(division_number);
			float_part = division_number - no_vote;
		}
		
		// make html
		var parent_id = this.attr('id');
		var ratings_html = '<ul class="ratings">';
		$('<ul class="ratings"></ul>').appendTo('#'+parent_id);
		for(i = 0; i < no_vote; i++)
		{
			ratings_html += '<li rel="'+i+'" class="'+settings.vote_class+'"></li>';
		}
		for(i = no_vote; i < settings.stars_number; i++)
		{
			ratings_html += '<li rel="'+i+'" class="'+settings.novote_class+'"></li>';
		}
		ratings_html += '</ul>';
		$('#'+parent_id).html(ratings_html);
		
		// calculate the floating part and insert the floating star to the DOM
		if(no_vote < settings.stars_number)
		{
			var element_width = $('#'+parent_id).children('ul.ratings li:eq(0)').width();
			var float_width = Math.floor(float_part*element_width);
			var offset = $('#'+parent_id+' ul.ratings li:eq('+no_vote+')').offset();
			var float_top = offset.top;
			var float_left = offset.left;
		}
		
		$('<li class="star_float"></li>').css({
			width:float_width+'px',
			position:'absolute',
			top:float_top+'px',
			left:float_left+'px',
			'z-index':1000
		}).addClass(settings.vote_class).appendTo('#'+parent_id+' ul.ratings:eq(0)');
		
		// bind click event to settings.onRate
		$('#'+parent_id+' ul.ratings li').unbind('click');
		$('#'+parent_id+' ul.ratings li').click(function(){
			value = parseInt($(this).attr('rel')) + 1;
			settings.onRate(value);
		});
		
		// process hover event
		$('#'+parent_id+' ul.ratings li').unbind('hover');
		$('#'+parent_id+' ul.ratings li').hover(function(){
			$('#'+parent_id+' ul.ratings li.star_float').hide();
			star_index = parseInt($(this).attr('rel')) + 1;
			$('#'+parent_id+' ul.ratings li:lt('+star_index+')')
				.addClass(settings.hover_class)
				.removeClass(settings.vote_class)
				.removeClass(settings.novote_class);
		}, function(){
			$('#'+parent_id+' ul.ratings li.'+settings.hover_class).removeClass(settings.hover_class);
			$('#'+parent_id+' ul.ratings li:lt('+no_vote+')').addClass(settings.vote_class);
			$('#'+parent_id+' ul.ratings li:gt('+no_vote+')').addClass(settings.novote_class);
			$('#'+parent_id+' ul.ratings li:eq('+no_vote+')').addClass(settings.novote_class);
			$('#'+parent_id+' ul.ratings li.star_float')
			.removeClass(settings.novote_class)
			.addClass(settings.vote_class)
			.show();
		});
	};
})
( jQuery );