<?php
	class ControllerCommonHeader extends Controller
	{
		public function index()
		{
			$this->load->model("core/media");
			$this->load->helper('image');
			$this->load->model("core/sitemap");
			$this->data['username'] = $this->session->data['username'];
			$this->data['sitename'] = $this->session->data['sitename'];
			$this->data['language'] = $this->getLanguageCBX();

			$this->data['route'] = explode('/', $_GET['route']);
			$this->data['addon'] = $this->getAddOnMenu("");
			$this->data['manag'] = array(
										"core/message" => "Message Management",
										"core/comment" => "Comment Management",
										"core/file" => "File Management",
										"core/category" => "Category Management",
										"core/member" => "Member Management",
										"core/user" => "User Management"
									);
			$this->data['account'] = array(
										"common/profile" => "My Profile",
										"common/changepassword" => "Change Password"
									);

			$imagepath = $this->model_core_media->getInformation("setting","brochure");
			
			if($imagepath != "")
			{
				$this->data['imagepreview'] = "<img class='png' src='".HelperImage::resizePNG($imagepath, 0, 80)."' >";
			}
			
			$this->data['title'] = $this->model_core_media->getInformation("setting", 'Title');
			$this->data['slogan'] = $this->model_core_media->getInformation("setting", 'Slogan');

			$this->id="header";
			$this->template="common/header.tpl";
			$this->render();
		}
		
		private function getLanguageCBX()
		{
			$this->load->model("common/control");
			$languages = $this->language->getLanguageList();
			$data = array();
			foreach($languages as $lang)
			{
				$data[$lang['code']] = $lang['name'];
			}
			
			$selectedvalue = $this->session->data['language'];
			return $this->model_common_control->getComboboxData("language", $data, $selectedvalue);
		}
		
		public function getAddOnMenu($parentid)
		{
			$siteid = $this->user->getSiteId();
			$str = "";
			
			$sitemaps = $this->model_core_sitemap->getListByParent($parentid, $siteid, "Addon");
			
			foreach($sitemaps as $item)
			{
				$childs = $this->model_core_sitemap->getListByParent($item['sitemapid'], $siteid, "Addon");
				
				$link = "<a>".$item['sitemapname']."</a>";
				
				if(substr($item['moduleid'],0,6) == "group/")
				{
					$item['moduleid'] = "module/information";
				}
				
				
				if($item['moduleid'] != "group" && $item['moduleid'] != "homepage")
				{
					$link='<a href="?route='.$item['moduleid']."&sitemapid=".$item['sitemapid'].'" title="'.$item['sitemapname'].'">'.$item['sitemapname'].'</a>';
				}
				
				$str .= "<li>";
				$str .= $link;
				
				if(count($childs) > 0)
				{			
					$str .= "<ul>";
					$str .= $this->getAddOnMenu($item['sitemapid']);
					$str .= "</ul>";
				}
				$str .= "</li>";
			}
			
			return $str;
			
		}
	}
?>