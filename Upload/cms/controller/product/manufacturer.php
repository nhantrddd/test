<?php
class ControllerProductManufacturer extends Controller
{
	private $error = array();
	
	function __construct()
	{
		$this->load->model("product/manufacturer");
		$this->load->helper('image');	
	}
   	
	public function index()
	{
		$this->load->model("product/manufacturer");
		$this->getList();
	}
	
	public function insert()
	{	
    	$this->getForm();
	}
	
	public function update()
	{

			$this->data['haspass'] = false;
			$this->data['readonly'] = 'readonly="readonly"';
			$this->data['class'] = 'readonly';
		
			$this->getForm();
	
  	}
	
	public function delete() 
	{
		$listid=$this->request->post['delete'];
		if(count($listid))
		{
			foreach($listid as $id)
			{
				$this->product_core_manufacturer->delete($id);	
			}
			$this->data['output'] = "true";
		}
		$this->id="content";
		$this->template="common/output.tpl";
		$this->render();
  	}
	
	private function getList() 
	{
		$data = $this->request->get;
		$this->data['insert'] = $this->url->http('product/manufacturer/insert');
		$this->data['delete'] = $this->url->http('product/manufacturer/delete');		
		
		$this->data['datas'] = array();
		$rows = $this->product_core_manufacturer->getList($where);
		//Page
		$page = $this->request->get['page'];		
		$x=$page;		
		$limit = 20;
		$total = count($rows); 
		// work out the pager values 
		$this->data['pager']  = $this->pager->pageLayout($total, $limit, $page); 
		
		$pager  = $this->pager->getPagerData($total, $limit, $page); 
		$offset = $pager->offset; 
		$limit  = $pager->limit; 
		$page   = $pager->page;
		
		for($i=$offset;$i < $offset + $limit && count($rows[$i])>0;$i++)
		//for($i=0; $i <= count($this->data['datas'])-1 ; $i++)
		{
			$this->data['datas'][$i] = $rows[$i];		
			if($this->data['datas'][$i]['imagepath'] != "")
			{
				$imagepreview = "<img width=100 src='".HelperImage::resizePNG($this->data['datas'][$i]['imagepath'], 180, 180)."' >";
			}
			$this->data['datas'][$i]['link_edit'] = $this->url->http('product/manufacturer/update&id='.$this->data['datas'][$i]['id']);
			$this->data['datas'][$i]['imagepreview'] = $imagepreview;
			$this->data['datas'][$i]['text_edit'] = "Sửa";
			
		}
		$this->data['refres']=$_SERVER['QUERY_STRING'];
		$this->id='content';
		$this->template="product/manufacturer_list.tpl";
		$this->layout="layout/center";
		
		$this->render();
	}
	
	
	private function getForm()
	{
		$this->data['DIR_UPLOADPHOTO'] = HTTP_SERVER."index.php?route=common/uploadpreview";
		$this->data['DIR_UPLOADATTACHMENT'] = HTTP_SERVER."index.php?route=common/uploadattachment";
			
		if ((isset($this->request->get['id'])) ) 
		{
      		$this->data['item'] = $this->product_core_manufacturer->getItem($this->request->get['id']);
			if($this->data['item']['imagepath'] != "")
			{
				//$imagepreview = "<img width=100 src='".HelperImage::resizePNG($this->data['item']['imagepath'],180,180)."' />";
				$imagepreview = HelperImage::resizePNG($this->data['item']['imagepath'],180,180);
			}
			$this->data['item']['imagepreview'] = $imagepreview;
    	}
		
		$this->id='content';
		$this->template='product/manufacturer_form.tpl';
		$this->layout="layout/center";
		
		$this->render();
	}
	
	public function save()
	{
		$data = $this->request->post;

		if($this->validateForm($data))
		{
			if($data['id']=="")
			{
				$this->product_core_manufacturer->insert($data);
					
			}
			else
			{
				$this->product_core_manufacturer->update($data);	
			}
			
			$this->data['output'] = "true";
		}
		else
		{
			foreach($this->error as $item)
			{
				$this->data['output'] .= $item."<br>";
			}
		}
		$this->id='content';
		$this->template='common/output.tpl';
		$this->render();
	}
	
	private function validateForm($data)
	{
		$this->load->model("product/manufacturer");
		if (trim($data['name']) == "") 
		{
      		$this->error['name'] = "Bạn chưa nhập tên nhà sản xuất";
    	}
		else if($data['id'] != "")
		{
			$where = "AND name = '".$data['name']."' AND id <> '".$data['id']."'";
			$manufacturers = $this->product_core_manufacturer->getList($where);
			
			if(count($manufacturers) > 0)
			{
				$this->error['name'] = "Tên nhà sản xuất đã tồn tại trong hệ thống";
			}
		}
		else
		{
			$where = "AND name = '".$data['name']."'";

			$manufacturers = $this->product_core_manufacturer->getList($where);

			if(count($manufacturers) > 0 && $data['id'] == "")
			{
				$this->error['name'] = "Tên nhà sản xuất đã tồn tại trong hệ thống";	
			}
		}
	
		if (count($this->error)==0) {
	  		return TRUE;
		} else {
	  		return FALSE;
		}
	}
	
	//Cac ham xu ly tren form
	
}
?>