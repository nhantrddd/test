<?php
class ControllerProductProduct extends Controller
{
	private $error = array();
	
	function __construct()
	{
		$this->load->model("product/product");
		$this->load->model("product/manufacturer");
		$this->load->helper('image');
		$this->load->model("core/category");
		$this->load->model("core/country");
		$this->load->model("common/control");
		$this->load->model("product/material");
		$this->load->model("core/sitemap");
		$this->data['dialog'] = $this->request->get['dialog'];
		$this->load->model("core/country");
		$this->data['zones'] = $this->model_core_country->getZonesByCode('VN');
		
		$data_manufactrurer = $this->model_product_manufacturer->getList();
		$this->data['cbmanufacturer'] = $this->model_common_control->getDataCombobox($data_manufactrurer, "name", "id");
		$data_sitemap = $this->model_core_sitemap->getListByParent("", $this->member->getSiteId());
		$this->data['cbsitmap'] = $this->model_common_control->getDataCombobox($data_sitemap, "sitemapname", "sitemapid");
		$this->data['sotien'] = array(
										'500000',
										'1000000',
										'2000000',
										'4000000',
										'5000000',
										'10000000',
										'200000000'
									);
	}
   	
	public function index()
	{
		$this->load->model("product/product");
		$this->getList();
	}
	
	public function insert()
	{	
    	$this->getForm();
	}
	
	public function update()
	{

			$this->data['haspass'] = false;
			$this->data['readonly'] = 'readonly="readonly"';
			$this->data['class'] = 'readonly';
		
			$this->getForm();
	
  	}
	
	public function delete() 
	{
		$listid=$this->request->post['delete'];
		if(count($listid))
		{
			foreach($listid as $id)
			{
				$this->model_product_product->delete($id);	
			}
			$this->data['output'] = "true";
		}
		$this->id="content";
		$this->template="common/output.tpl";
		$this->render();
  	}
	
	private function getList() 
	{
		$refersitemap = $this->request->get['sitemapid'];
		$data = $this->request->get;
		//$this->data['cancel'] = $this->url->http('module/product&sitemapid='.$this->request->get['sitemapid']);
		$this->data['insert'] = $this->url->http('module/product/insert&sitemapid='.$this->request->get['sitemapid']);
		$this->data['delete'] = $this->url->http('product/delete');
		
		$this->data['datas'] = array();
		if($this->request->get['dialog'] == true){
			$where .= " AND refersitemap like '%[phukien]%'";
			$rows = $this->model_product_product->getList($where);
		}else
		{
			//refer sitemap
			/*$data_sitemap = array();
			$this->model_core_sitemap->getTreeSitemap($refersitemap, &$data_sitemap, $this->user->getSiteId());
		
			foreach($data_sitemap as $item)
			{
				$arr[] = " refersitemap like '%[".$item['sitemapid']."]%'";
			}
			$where .= "AND (". implode($arr," OR ").")";*/
			$where = " AND refersitemap like '%[".$refersitemap."]%'";
			$para = $_GET;
			if(count($para))
			foreach($para as $key => $val)
			{
				$para[$key]	= urldecode($val);
			}
			if(trim($para['productname']))
			{
				$where .= " AND productname like '%".$para['productname']."%'";
			}
			if(trim($para['manufacturer']))
			{
				$where .= " AND manufacturerid = '".$para['manufacturer']."'";
			}
			if($para['giatu'])
			{
				$where .= " AND price >='".$para['giatu']."'";	
			}
			
			if($para['giaden'])
			{
				$where .= " AND price <='".$para['giaden']."'";	
			}
			
			
			
			
			$rows = $this->model_product_product->getList($where);
		}
		//Page
		$page = $this->request->get['page'];		
		$x=$page;		
		$limit = 20;
		$total = count($rows); 
		// work out the pager values 
		$this->data['pager']  = $this->pager->pageLayout($total, $limit, $page); 
		$_SESSION['return']= $_SERVER['QUERY_STRING'];
		$pager  = $this->pager->getPagerData($total, $limit, $page); 
		$offset = $pager->offset; 
		$limit  = $pager->limit; 
		$page   = $pager->page;
		
		for($i=$offset;$i < $offset + $limit && count($rows[$i])>0;$i++)
		//for($i=0; $i <= count($this->data['datas'])-1 ; $i++)
		{
			$this->data['datas'][$i] = $rows[$i];		
			if($this->data['datas'][$i]['imagepath'] != "")
			{
				$imagepreview = "<img width=100 src='".HelperImage::resizePNG($this->data['datas'][$i]['imagepath'], 180, 180)."' >";
			}
			$this->data['datas'][$i]['link_edit'] = $this->url->http('module/product/update&id='.$this->data['datas'][$i]['id'].'&sitemapid='.$this->request->get['sitemapid']);
			$this->data['datas'][$i]['imagepreview'] = $imagepreview;
			$this->data['datas'][$i]['text_edit'] = "Sửa";
			
			$country = $this->model_core_country->getCountry($this->data['datas'][$i]['countryid']);
			$this->data['datas'][$i]['countryname'] = $country['countryname'];
			
			$manufacturer = $this->model_product_manufacturer->getItem($this->data['datas'][$i]['manufacturerid']);
			$this->data['datas'][$i]['manufacturername'] = $manufacturer['name'];
			
			$material = $this->model_product_material->getItem($this->data['datas'][$i]['materialid']);
			$this->data['datas'][$i]['materialname'] = $material['materialname'];
			
			$arraccessories = array();
			$arraccessories = split(",",$this->data['datas'][$i]['accessories']);
			$this->data['datas'][$i]['accessories'] = array();
			foreach($arraccessories as $arraccessory)
			{
				$this->data['datas'][$i]['accessories'][] = $this->model_product_product->getItem($arraccessory,"");
				//$this->data['datas'][$i]['accessories'][] = $arritem;
			}
						
		}
		$this->data['refres']=$_SERVER['QUERY_STRING'];
		$this->id='content';
		$this->template="product/product_list.tpl";
		

		$this->render();
	}
	
	
	private function getForm()
	{
		$this->data['DIR_UPLOADPHOTO'] = HTTP_SERVER."index.php?route=common/uploadpreview";
		$this->data['DIR_UPLOADATTACHMENT'] = HTTP_SERVER."index.php?route=common/uploadattachment";
		
		$this->data['zonesto'] = array();
		$this->data['properties'] = array();
		$this->data['statuspro'] = array();
		$this->model_core_category->getTree("status",$this->data['statuspro']);
		unset($this->data['statuspro'][0]);
		
		//print_r($this->data['statuspro']);
		
		$this->data['ctkhuyenmai'] = array();
		$this->model_core_category->getTree("ctkhuyenmai",$this->data['ctkhuyenmai']);
		unset($this->data['ctkhuyenmai'][0]);
		
		/*$data_cbcountry = $this->model_core_country->getCountrys();
		$this->data['country'] = $this->model_common_control->getDataCombobox($data_cbcountry,'countryname','countryid',$select);
		
		$data_cbmanufacturer = $this->model_product_manufacturer->getList();
		$this->data['manufacturer'] = $this->model_common_control->getDataCombobox($data_cbmanufacturer,'name','id',$select);*/
		
		if ((isset($this->request->get['id'])) ) 
		{
      		$this->data['item'] = $this->model_product_product->getItem($this->request->get['id']);
			
			$this->data['properties'] = $this->string->referSiteMapToArray($this->data['item']['groupkeys']);
			
			$product=$this->data['item'];
			
			if($this->data['item']['imagepath'] != "")
			{
				//$imagepreview = "<img width=100 src='".HelperImage::resizePNG($this->data['item']['imagepath'],180,180)."' />";
				$imagepreview = HelperImage::resizePNG($this->data['item']['imagepath'],180,180);
			}
			$this->data['item']['imagepreview'] = $imagepreview;
			$arraccessories = split(",",$this->data['item']['accessories']);
					
			$this->data['item']['accessories'] = array();
			foreach($arraccessories as $arraccessory)
			{
				$this->data['item']['accessories'][] = $this->model_product_product->getItem($arraccessory,"");
				//$this->data['datas'][$i]['accessories'][] = $arritem;
			}
			
			
			
			//sub image
			
			$subimage=explode(",",$product['subimageid']);
			//print_r($subimage);
					
				$this->data['attachment']=array();
				foreach($subimage as $key => $item)
				{
					$this->data['attachment'][$key] = $this->model_core_file->getFile($item);
					$this->data['attachment'][$key]['subimg'] = HelperImage::resizePNG($this->data['attachment'][$key]['filepath'], 50, 50);
					if(!$this->string->isImage($this->data['attachment'][$key]['extension']))
						$this->data['attachment'][$key]['subimg'] = DIR_IMAGE."icon/dinhkem.png";
				}
					
		
			
				
			
    	}
		
		if($this->data['item']['imagepreview'] == "")
		{
			$this->data['item']['imagepreview'] = "view/skin1/image/selectimage.png";
		}
		
		$data_cbcountry = $this->model_core_country->getCountrys();
		$this->data['country'] = $this->model_common_control->getDataCombobox($data_cbcountry,'countryname','countryid',$this->data['item']['countryid']);
		
		$data_cbmanufacturer = $this->model_product_manufacturer->getList();
		$this->data['manufacturer'] = $this->model_common_control->getDataCombobox($data_cbmanufacturer,'name','id',$this->data['item']['manufacturerid']);
		
		$data_cbmaterial = $this->model_product_material->getList("Order by materialname");
		$this->data['material'] = $this->model_common_control->getDataCombobox($data_cbmaterial,'materialname','id',$this->data['item']['materialid']);
				
		$this->data['listReferSiteMap'] = $this->getListSiteMapCheckBox($this->data['item']);
		
		/*####################################################*/
		$str = "";
		if($this->data['item']['tourtype'] == 'nuocngoai'){
			$country = $this->model_core_country->getCountrys(" AND countryid <> '230'");
			foreach($country as $item){
				$str .= "<optgroup label='".$item['countryname']."'>";
				$zones = $this->model_core_country->getZones(" AND countryid = '".$item['countryid']."'");
				if(count($zones)){
					foreach($zones as $z){
						$str .= "<option value='".$z['zoneid']."'";
						if($this->data['item']['zoneto'] == $z['zoneid']){
							$str .=  "selected='selected'";
						}
						
						$str .= ">".$z['zonename']."</option>";
					}
				}
                $str .= "</optgroup>";
			}
			$this->data['zonesto'] = $str;
			//$this->data['zonesto'] = $this->model_core_country->getZones(" AND countryid <> '230'");
		}
		else{
			$zones = $this->data['zones'];
			if(count($zones)){
				foreach($zones as $z){
					$str .= "<option value='".$z['zoneid']."'";
					if($this->data['item']['zoneto'] == $z['zoneid']){
						$str .=  "selected='selected'";
					}
					
					$str .= ">".$z['zonename']."</option>";
				}
			}
			$this->data['zonesto'] = $str;
		}
		
		$this->id='content';
		$this->template='product/product_form.tpl';
		//$this->layout="layout/center";
		
		$this->render();
	}
	
	private function getListSiteMapCheckBox($product)
	{
		$strReferSitemap = $product['refersitemap'];
		$sitemapid = $this->request->get['sitemapid'];
		if($strReferSitemap == "")
		{
			$strReferSitemap = "[".$sitemapid."]";
		}
		$route = $this->getRoute();

		$list = $this->model_core_sitemap->getListByModule("module/product",$this->user->getSiteId());
		
		
		$html = "";
		foreach($list as $item)
		{
			$html .= '<tr>';
			$html .= '<td>';
			
			$sitemapid = "[".$item['sitemapid']."]";
			
			$pos = strrpos($strReferSitemap, $sitemapid);
			if ($pos === false) {
				$checked = "";
			}else{
				$checked = "checked=checked";
			}
			$html .= '<label><input name="listrefersitemap['.$item['sitemapid'].']" type="checkbox" value="'.$item['sitemapid'].'" '.$checked.'/> '.$item['sitemapname'].'</label>';
			$path = $this->model_core_sitemap->getBreadcrumb($item['sitemapid'], $this->user->getSiteId());
			$path = strip_tags($path);
			$html .= '</td><td>'.$path.'</td> </tr>';
		}
		
		return $html;
	}
	public function updatePos()
	{
		$data = $this->request->post;
		$arr_pos = $data['position'];
		
		if(count($arr_pos))
		{
			foreach($arr_pos as $id =>$pos)
			{
				$this->model_product_product->updateCol($id,'position',$pos);
			}
		}
		$this->data['output'] = "true";
		$this->id='postlist';
		$this->template='common/output.tpl';	
		$this->render();
	}
	public function save()
	{
		$data = $this->request->post;	
		$data['priceold'] = $this->string->toNumber($data['priceold']);		
		$data['price'] = $this->string->toNumber($data['price']);
		$country = $this->model_core_country->getCountry($data['countryid']);
		$data['countryname'] = $country['countryname'];
		$manufacturer = $this->model_product_manufacturer->getItem($data['manufacturerid']);
		$data['manufacturername'] = $manufacturer['name'];
		$material = $this->model_product_material->getItem($data['materialid']);
		$data['materialname'] = $material['materialname'];
		$data['refersitemap'] = $this->string->arrayToString($data['listrefersitemap']);
		if(count($data['accessoryid']) != 0)
		{
			$data['accessories'] = implode(",", $data['accessoryid']);
		}
		
		//sale off price
		$data['productparent'] = $data['productparent'];
		$data['mediatype'] = $data['mediatype'];
		$data['userid'] = $this->user->getId();
		$data['updatedate'] = $this->date->getToday();
		$data['salenumber'] = $this->string->toNumber($data['salenumber']);
		$data['startdate'] = $this->date->formatMySQLDateQuery($data['startdate'], 'YMD', '-');
		
		//save subimage
/*		$listAttachment=$this->data['attimageid'];
		$this->model_product_product->saveAttachment($data['id'],$listAttachment);
		$listdelfile=$this->data['delfile'];
		if(count($listdelfile))
			foreach($listdelfile as $item)
				$this->model_core_file->deleteFile($item);
		$this->model_product_product->clearTempFile();*/
		
		if(count($data['attimageid']) != 0)
		{
			$data['subimageid'] = implode(",", $data['attimageid']);
		}
		
		$data['groupkeys'] = $this->getProperties($data['loaisp']);
	
		if($this->validateForm($data))
		{	
			if($data['id']=="")
			{
				$this->model_product_product->insert($data);
				
			}
			else
			{	
				$this->model_product_product->update($data);	
			}
			
			$this->data['output'] = "true";
		}
		else
		{
			foreach($this->error as $item)
			{
				$this->data['output'] .= $item."<br>";
			}
		}
		$this->id='content';
		$this->template='common/output.tpl';
		$this->render();
	}
	
	private function getProperties($data)
	{
		$groupkeys = $this->string->arrayToString($data);
		return $groupkeys;	
	}
	
	private function validateForm($data)
	{
		$this->load->model("product/product");
		if (trim($data['productname']) == "") 
		{
      		$this->error['name'] = "Bạn chưa nhập tên sản phẩm";
    	}
		
		/*else if($data['id'] != "")
		{
			$where = "AND name = '".$data['name']."' AND id <> '".$data['id']."'";
			$manufacturers = $this->model_product_product->getList($where);
			
			if(count($manufacturers) > 0)
			{
				$this->error['name'] = "Tên nhà sản xuất đã tồn tại trong hệ thống";
			}
		}
		else
		{
			$where = "AND name = '".$data['name']."'";

			$manufacturers = $this->model_product_product->getList($where);

			if(count($manufacturers) > 0 && $data['id'] == "")
			{
				$this->error['name'] = "Tên nhà sản xuất đã tồn tại trong hệ thống";	
			}
		}*/
	
		if (count($this->error)==0) {
	  		return TRUE;
		} else {
	  		return FALSE;
		}
	}
	
	//Cac ham xu ly tren form
	public function getProduct()
	{
		$col = $this->request->get['col'];
		$val = $this->request->get['val'];
		$operator = $this->request->get['operator'];
		if($operator == "")
			$operator = "equal";
		
		$where = "";
		switch($operator)
		{
			case "equal":
				$where = " AND ".$col." = '".$val."'";
				break;
			case "like":
				$where = " AND ".$col." like '%".$val."%'";
				break;
			case "other":
				$where = " AND ".$col." <> '".$val."'";
				break;
			case "in":
				$where = " AND ".$col." in  (".$val.")";
				break;	
		}
		
			
		
		$datas = $this->model_product_product->getList($where);
		foreach($datas as $key => $item)
		{
			$product = $this->model_product_product->getItem($item['id']);
			$datas[$key]['productname'] = $product['productname'];
			$imagepreview = HelperImage::resizePNG($product['imagepath'],180,180);
			$datas[$key]['imagepreview'] = $imagepreview;
			$datas[$key]['manufacturername'] = $product['manufacturername'];
			$country = $this->model_core_country->getCountry($product['countryid']);
			$datas[$key]['countryname'] = $country['countryname'];
			$datas[$key]['materialname'] = $product['materialname'];
			$datas[$key]['price'] = $product['price'];
		}
			
		
		$this->data['output'] = json_encode(array('products' => $datas));
		$this->id="product";
		$this->template="common/output.tpl";
		$this->render();
	}
	
	//Sale off
	
	public function loadPrice()
	{
		$this->load->model("core/media");
		$this->load->helper('image');
		$mediaid = $this->request->get['mediaid'];
		
		//load price
		$this->data['child']=$this->model_product_product->getListByParent($mediaid," AND mediatype = 'price' Order by position");
		
		$this->id='post';
		$this->template='product/saleoff.tpl';	
		$this->render();
	}
	
	public function getPrice()
	{
		$this->load->model("product/product");
		$this->load->helper('image');
		$id = $this->request->get['mediaid'];
		$media=$this->model_product_product->getItem($id);
		
		$this->data['output'] = json_encode(array('price' => $media));
		$this->id='post';
		$this->template="common/output.tpl";
		$this->render();
	}
	
	
	public function selectzoneto()
	{
		$tourtype = $this->request->get['tourtype'];		
		/*####################################################*/
		$str = "";
		if($tourtype == 'nuocngoai'){
			$country = $this->model_core_country->getCountrys(" AND countryid <> '230'");
			foreach($country as $item){
				$str .= "<optgroup label='".$item['countryname']."'>";
				$zones = $this->model_core_country->getZones(" AND countryid = '".$item['countryid']."'");
				if(count($zones)){
					foreach($zones as $z){
						$str .= "<option value='".$z['zoneid']."'";
						if($this->data['item']['zoneto'] == $z['zoneid']){
							$str .=  "selected='selected'";
						}
						
						$str .= ">".$z['zonename']."</option>";
					}
				}
                $str .= "</optgroup>";
			}
			$this->data['zonesto'] = $str;
			//$this->data['zonesto'] = $this->model_core_country->getZones(" AND countryid <> '230'");
		}
		else{
			$str = "";
			$zones = $this->data['zones'];
			if(count($zones)){
				foreach($zones as $z){
					$str .= "<option value='".$z['zoneid']."'";
					if($this->data['item']['zoneto'] == $z['zoneid']){
						$str .=  "selected='selected'";
					}
					
					$str .= ">".$z['zonename']."</option>";
				}
			}
			$this->data['zonesto'] = $str;
		}
		$this->data['output'] = $this->data['zonesto'];
		$this->id='post';
		$this->template="common/output.tpl";
		$this->render();
	}
	
	public function deleteProduct()
	{
		$this->load->model("product/product");
		$this->load->helper('image');
		$id = $this->request->get['mediaid'];
		$this->model_product_product->deleteProduct($id);
		
		$this->id='post';
		$this->template="common/output.tpl";
		$this->render();
	}
	
	public function updatePosChild()
	{
		$this->load->model("product/product");
		$data = $this->request->post;
		
		$this->model_product_product->updatePosChild($data);
		
		$this->data['output'] = "true";
		$this->template="common/output.tpl";
		$this->render();
	}
}
?>