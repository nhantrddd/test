<?php
class ControllerAddonPopup extends Controller
{
    public function __construct()
    {
        /*
         * Load resources
         */
        $this->load->language("addon/popup");
        $this->load->model("core/media");
    }
    
    public function index()
    {
        /*
         * Get media id
         */
        $mediaId = "popup";
        
        /*
         * Get popup detail
         */
        $attr   = $this->model_core_media->getInformation($mediaId, "attr");
        $detail = $this->model_core_media->getInformation($mediaId, "detail");
        
        /*
         * Decode
         */
        $item   = array();
        $attr  = (trim($attr) != "") ? (array)json_decode($attr) : false;
        
        if($attr !== false && is_array($attr))
        {
            $item["id"]     = isset($attr["id"]) ? $attr["id"] : 0;
            $item["title"]  = isset($attr["title"]) ? $attr["title"] : "";
            $item["width"]  = isset($attr["width"]) ? (int)$attr["width"] : 300;
            $item["height"] = isset($attr["height"]) ? (int)$attr["height"] : 300;
            $item["number"] = isset($attr["number"]) ? (int)$attr["number"] : 1;
            $item["status"] = isset($attr["status"]) ? (int)$attr["status"] : 0;
            $item["detail"] = $detail;
        }
        
        $this->data["item"] = $item;
        
        /*
         * Assign language
         */
        $this->data = array_merge($this->data, $this->language->getData());
        
        /*
         * Set view
         */
        $this->id       = "content";
        $this->template = "addon/popup.tpl";
        $this->layout   = "layout/center";
        $this->render();
    }
    
    public function save()
    {
        if(is_array($this->request->post) && count($this->request->post) > 0)
        {
            /*
             * Get media id
             */
            $mediaId = "popup";

            /*
             * Get data post
             */
            $data   = $this->request->post;
            $detail = isset($data["detail"]) ? trim($data["detail"]) : "";
            
            if(isset($data["detail"]))
            {
                unset($data["detail"]);
            }
            
            $attr = (is_array($data) && count($data) > 0) ? json_encode($data) : "";
            
            /*
             * Save
             */
            $this->model_core_media->saveInformation($mediaId, "attr", $attr);
            $this->model_core_media->saveInformation($mediaId, "detail", $detail);
        }
        
        /*
         * Redirect form
         */
        $this->redirect("?route=addon/popup");
    }
}
?>