<?php
class ControllerAddonNhantin extends Controller
{
	private $error = array();
   	function __construct() 
	{
		if(!$this->user->hasPermission($this->getRoute(), "access"))
		{
			$this->response->redirect("?route=common/permission");
		}
		$this->data['permissionAdd'] = true;
		$this->data['permissionEdit'] = true;
		$this->data['permissionDelete'] = true;
		if(!$this->user->hasPermission($this->getRoute(), "add"))
		{
			$this->data['permissionAdd'] = false;
		}
		if(!$this->user->hasPermission($this->getRoute(), "edit"))
		{
			$this->data['permissionEdit'] = false;
		}
		if(!$this->user->hasPermission($this->getRoute(), "delete"))
		{
			$this->data['permissionDelete'] = false;
		}
		
		$this->load->model("ben/nhantin");
		
   	}
	
	public function index()
	{
		$this->getList();
	}
	
	private function getList()
	{
		$this->data['datas'] = $this->model_ben_nhantin->getList();
		
		$this->id='content';
		$this->template="addon/nhantin_list.tpl";
		$this->layout="layout/center";
		$this->render();	
	}
	
	public function exportData()
	{
		require_once(DIR_COMPONENT."phpexcel/PHPExcel.php");
		require_once(DIR_COMPONENT."phpexcel/PHPExcel/RichText.php");
		require_once(DIR_COMPONENT."phpexcel/PHPExcel/IOFactory.php");
		$this->load->model("ben/nhantin");
		
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		// Set properties
		$objPHPExcel->getProperties()->setCreator("VNT")
									 ->setLastModifiedBy("VNT")
									 ->setTitle("Office 2007 XLSX Test Document")
									 ->setSubject("Office 2007 XLSX Test Document")
									 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
									 ->setKeywords("office 2007 openxml php")
									 ->setCategory("VNT");
		
		$column_array = array(
				'1'		=> 'A',
				'2'		=> 'B',
				'3'		=> 'C',
				'4'		=> 'D',
				'5'		=> 'E',
				'6'		=> 'F',
				'7'		=> 'G',
				'8'		=> 'H',
				'9'		=> 'I',
				'10'	=> 'J',
				'11'	=> 'K',
				'12'	=> 'L',
				'13'	=> 'M',
				'14'	=> 'N',
				'15'	=> 'O',
				'16'	=> 'P',
				'17'	=> 'Q',
				'18'	=> 'R',
				'19'	=> 'S',
				'20'	=> 'T',
				'21'	=> 'U',
				'22'	=> 'V',
				'23'	=> 'W',
				'24'	=> 'X',
				'25'	=> 'Y',
				'26'	=> 'Z'
		);
		
		$objPHPExcel->setActiveSheetIndex(0);
		
		// set column width
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(70);

		
		// Add some data
		$objPHPExcel->getActiveSheet()
					->setCellValue('A1', 'Email');
					
		///////////////////////////////////////////////////////////////////////////////////
		// ghi dữ liệu vào DB
		$file_name = "export".date("now");
		$to = 0;
		$row = 2;
		
/*		$arrsitemap = $this->model_core_sitemap->getListByParent($sitemapid,"default");
		
		foreach($arrsitemap as $item){
			$listsitemapid[] = $item['sitemapid'];
		}
		
		$listsitemapid[] = $sitemapid;
				
		$queryoptions = array();
		$queryoptions['mediaparent'] = '%';
		$queryoptions['mediatype'] = '%';
		$queryoptions['refersitemap'] = $listsitemapid;*/
		
		$medias = array();
		
		$medias = $this->model_ben_nhantin->getList();
		
		if($medias){
			//load child price

			//$sqlview = mysql_query("select * from art_sp where c_id in ($str) order by sort ASC, a_id ASC");
			foreach ($medias as $record)
			{
				$objPHPExcel->getActiveSheet()
						->setCellValue("A$row", $record['email']);
						//->setCellValue("D$row", $record['a_price_usd'])
						//->setCellValue("E$row", $record['a_price'])
						//->setCellValue("F$row", $record['a_giam']);
				$row++;
			}
		}
		
		// Rename sheet
		$objPHPExcel->getActiveSheet()->setTitle($file_name);
		
		ob_end_clean();
		// Redirect output to a client’s web browser (Excel5)
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		header('Content-Type: application/vnd.ms-excel');
		header("Content-Disposition: attachment;filename=$file_name.xls");
		header('Cache-Control: max-age=0');
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		//$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
		$objWriter->save('php://output');
		//$objWriter->save('$file_name.xls');
	}
}
?>