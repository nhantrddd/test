<?php
$_["text_popup_header"]     = "Chỉnh sửa popup";
$_["text_popup_title"]      = "Tiêu đề";
$_["text_popup_status"]     = "Trạng thái";
$_["text_popup_width"]      = "Chiều rộng";
$_["text_popup_height"]     = "Chiều cao";
$_["text_popup_detail"]     = "Chi tiết";
$_["text_popup_number"]     = "Lần hiện";
$_["text_popup_active"]     = "Bật";
$_["text_popup_inactive"]   = "Tắt";
?>