<?php
class ControllerSitebarZone extends Controller
{
	public function index()
	{
		$this->load->model('core/product');
		$this->load->model('core/country');
		$this->data['tourtype'] = "Trong nước";
		
		$where = " AND (zonefrom <> '' OR zoneto <> '')";
		$arr_product = $this->model_core_product->getList($where);
		$arr_zone = array();
		if(count($arr_product)){
			foreach($arr_product as $product){
				$arr_zone[] = "'".$product['zonefrom']."'";
				$arr_zone[] = "'".$product['zoneto']."'";
			}
		}
		$arr_zone = array_unique($arr_zone);
		$where = "AND countryid = 230 AND zoneid IN (". implode($arr_zone," , ").")";
		$this->data['zones'] = $this->model_core_country->getListZone($where);
		
		$this->id="content";
		$this->template="sitebar/zone.tpl";
		$this->render();
	}
	
	public function zone_nuocngoai()
	{
		$this->load->model('core/product');
		$this->load->model('core/country');
		$this->data['tourtype'] = "Nước ngoài";
		
		$where = " AND tourtype = 'nuocngoai'";
		$where .= " AND (zonefrom <> '' OR zoneto <> '')";
		$arr_product = $this->model_core_product->getList($where);
		$arr_zone = array();
		if(count($arr_product)){
			foreach($arr_product as $product){
				//$arr_zone[] = "'".$product['zonefrom']."'";
				$arr_zone[] = "'".$product['zoneto']."'";
			}
		}
		$arr_zone = array_unique($arr_zone);
		if(count($arr_zone)){
			$where = " AND zoneid IN (". implode($arr_zone," , ").")";
		}
		else{
			$where = " AND 1=2";
		}
		$this->data['zones'] = $this->model_core_country->getListZone($where);
		
		$this->id="content";
		$this->template="sitebar/zone.tpl";
		$this->render();
	}
}
?>