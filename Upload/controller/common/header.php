<?php
class ControllerCommonHeader extends Controller
{
	public function index()
	{
		$siteid = $this->member->getSiteId();		
		$sitemapid = $this->document->sitemapid;
		
		$this->load->model("core/media");
		$this->data['sitemap'] = $this->model_core_sitemap->getItem($sitemapid, $siteid);
		//$this->data['media'] = $this->model_core_media->getItem($siteid.$sitemapid);
		//$this->data['supportonline'] = html_entity_decode($this->data['media']['description']);
		$this->data['mainmenu'] = $this->getMenu("");
		$this->data['searchtour'] = $this->loadModule('addon/searchtour');
		//Banner home
		$template = array(
					  'template' => "home/bannerhome.tpl",
					  'width' => 587,
					  'height' =>257
					  );	
		$arr = array("banner-home-page",0,"",$template);		
		$this->data['bannerhome'] = $this->loadModule('module/block','getLink',$arr);
		
		$this->id="header";
		$this->template="common/header.tpl";
		$this->render();
	}
	
	public function getMenu($parentid)
	{
		$this->load->model("core/sitemap");
		
		$siteid = $this->member->getSiteId();
		
		$rootid = $this->model_core_sitemap->getRoot($this->document->sitemapid, $siteid);

		if($this->document->sitemapid == "")
			$rootid = 'trang-chu';
		$str = "";
		
		
		$sitemaps = $this->model_core_sitemap->getListByParent($parentid, $siteid);
		
		foreach($sitemaps as $item)
		{
			$childs = $this->model_core_sitemap->getListByParent($item['sitemapid'], $siteid);
			
			$currenttab = "";
			if($item['sitemapid'] == $rootid)
				$currenttab = "class='current-tab'";
			
			$link = "<a ".$currenttab." href='".$this->document->createLink($item['sitemapid'])."'>".$item['sitemapname']."</a>";
			
			
			if($item['sitemapid'] == "tour-du-lich"){
				$link = "<a id='tour-du-lich' ".$currenttab." href='".$this->document->createLink($item['sitemapid'])."'>".$item['sitemapname']."</a>";
				$link .= "<ul id='tour-du-lich-child' onmouseout='tour_close()'>";
				$link .= "<li><a href='".HTTP_SERVER."tour/trongnuoc.html'>Tour trong nước"."</a></li>";
				$link .= "<li><a href='".HTTP_SERVER."tour/nuocngoai.html'>Tour nước ngoài"."</a></li>";
				$link .= "</ul>";
			}
				
			if($item['moduleid'] == "group")
			{
				$link = "<a ".$currenttab." href='index.php?route=page/detail&sitemapid=".$item['sitemapid']."'>".$item['sitemapname']."</a>";
			}
			if($item['moduleid'] == "homepage"){
				$link = "<a ".$currenttab." href='".HTTP_SERVER."'>".$item['sitemapname']."</a>";
			}
			
			$str .= "<li><span class='ben-char ben-left'></span>";
			$str .= $link;
			
			if(count($childs) > 0)
			{
				$str .= "<ul>";
				$str .= $this->getMenu($item['sitemapid']);
				$str .= "</ul>";
			}

			$str .= "</li>";
		}
		
		return $str;
		
	}
}
?>