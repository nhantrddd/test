<?php
class ControllerCommonBanner extends Controller
{
	public function index()
	{
		$this->load->model("core/sitemap");
		$this->data['sitemaps'] = $this->model_core_sitemap->getListByParent("",$this->member->getSiteId());
		$this->data['menus'] = array();
		foreach($this->data['sitemaps'] as $key => $sitemap)
		{
			$submenu = $this->model_core_sitemap->getListByParent($sitemap['sitemapid'],$this->member->getSiteId());
			if(count($submenu))
			{
				$this->data['sitemaps'][$key]['subname'] = "";
				$arr = array();
				for($i=0;$i<2;$i++)
				{
					$arr[] = $submenu[$i]['sitemapname'];
				}
				$this->data['sitemaps'][$key]['subname'] = implode(", ",$arr);
				$arr = array($sitemap['sitemapid']);
				$this->data['sitemaps'][$key]['submenu'] = $this->loadModule('common/category','banner',$arr);
			}
		}
		
		//load banner ở trang chủ
		$template = array(
					  'template' => "home/bannerhome.tpl",
					  'width' => 740,
					  'height' => 297
					  );	
		$arr = array("bannerhome",0,"",$template);
		$this->data['bannerhome'] = $this->loadModule('module/block','getLink',$arr);
		

		//$this->data['tuvanonline'] = $this->loadModule('addon/tuvanonline');	
		
		$this->id="header";
		$this->template="common/banner.tpl";
		$this->render();
	}
}
?>