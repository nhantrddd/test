<?php
	class ControllerCommonFooter extends Controller
	{
		public function index()
		{
			$this->data['footer'] = $this->loadModule("addon/footer");
			//$this->data['footercategory'] = $this->loadModule("addon/footercategory");
			$this->data['footermenu'] = $this->getMainMenu("");
                        
                        /*
                         * Load popup
                         */
                        $this->data['popup'] = $this->loadModule("addon/popup");
                        
			$this->id="footer";
			$this->template="common/footer.tpl";
			$this->render();
		}
		
		public function getMainMenu($parentid)
		{
			$this->load->model("core/sitemap");
			
			$siteid = $this->member->getSiteId();
			
			$rootid = $this->model_core_sitemap->getRoot($this->document->sitemapid, $siteid);
	
			if($this->document->sitemapid == "")
				$rootid = 'trangchu';
			$str = "";		
			
			$sitemaps = $this->model_core_sitemap->getListByParent($parentid, $siteid);
			
			//foreach($sitemaps as $item)
			for($i = 0; $i < count($sitemaps); $i++)
			{
				$currenttab = "";
				if($sitemaps[$i]['sitemapid'] == $rootid)
					$currenttab = "class='current-tab'";
					
				if($sitemaps[$i]['moduleid'] == "homepage"){
					$link = '<a href="'.HTTP_SERVER.'">'.$sitemaps[$i]['sitemapname'].'</a>';
				}
				else{
					$link = '<a href="'.$this->document->createLink($sitemaps[$i]["sitemapid"]).'">'.$sitemaps[$i]['sitemapname'].'</a>';
				}
				$link .= ' &nbsp;&nbsp;|&nbsp;&nbsp; ';
				$str .= $link;
			}		
			//$str .= '<a href="'.$this->document->createLink($sitemaps[count($sitemaps)-1]["sitemapid"]).'">'.$sitemaps[count($sitemaps)-1]['sitemapname'].'</a>';
			return $str;		
		}
	}
?>