<?php
class ControllerCommonCategory extends Controller
{
	public function index($sitemapid = "")
	{
		$this->load->model("core/sitemap");
		$this->data['sitemaps'] = $this->model_core_sitemap->getListByParent("",$this->member->getSiteId());
		$this->data['menus'] = array();
		foreach($this->data['sitemaps'] as $key => $sitemap)
		{
			$submenu = $this->model_core_sitemap->getListByParent($sitemap['sitemapid'],$this->member->getSiteId());
			$this->data['sitemaps'][$key]['subname'] ="";
			$arr = array();
			for($i=0;$i<2;$i++)
			{
				$arr[] = $submenu[$i]['sitemapname'];
			}
			$this->data['sitemaps'][$key]['subname'] = implode(", ",$arr);
			$arr = array($sitemap['sitemapid']);
			$this->data['sitemaps'][$key]['submenu'] = $this->loadModule('common/category','banner',$arr);
		}
		
				
		$this->id="header";
		$this->template="common/category.tpl";
		$this->render();
	}
	
	public function banner($sitemapid = "")
	{
		$this->load->model("core/sitemap");
		
		$this->data['output'] = "<ul>".$this->getMenu($sitemapid)."</ul>";
		
				
		$this->id="header";
		$this->template="common/output.tpl";
		$this->render();
	}
	
	public function getMenu($parentid)
	{
		
		
		$siteid = $this->member->getSiteId();
		
		$rootid = $this->model_core_sitemap->getRoot($this->document->sitemapid, $siteid);

		if($this->document->sitemapid == "")
			$rootid = 'homepage';
		$str = "";
		
		
		$sitemaps = $this->model_core_sitemap->getListByParent($parentid, $siteid);
		foreach($sitemaps as $item)
		{
			$childs = $this->model_core_sitemap->getListByParent($item['sitemapid'], $siteid);
			
			
			$link = "<a  href='".$this->document->createLink($item['sitemapid'])."'>".$item['sitemapname']."</a>";
			
			
			$str .= "<li> <div class='collape'>";
			$str .= $link;
			
			if(count($childs) > 0)
			{
				$str .= "<ul>";
				$str .= $this->getMenu($item['sitemapid']);
				$str .= "</ul>";
			}

			$str .= "</li>";
		
		}
		return $str;
		
		
		
		
		
	}	
	
	
	
	
	
	
	
	
	
	
	
}
?>