<?php 
class ControllerAddonFooter extends Controller
{
	public function index()
	{
		$this->load->model("core/media");
		$this->data['media'] = $this->model_core_media->getItem($this->member->getSiteId()."footer");
		
		$this->id="footer";
		$this->template = "addon/footer.tpl";
		$this->render();
	}	
}
?>