<?php 
class ControllerAddonHome extends Controller
{
	public function index()
	{
		$this->load->model("core/media");
		$this->data['media'] = $this->model_core_media->getItem($this->member->getSiteId()."home");
		
		$this->id="home";
		$this->template = "addon/home.tpl";
		$this->render();
	}	
}
?>