<?php
class ControllerAddonSearchtour extends Controller
{
	private $error = array();
	public function index()
	{
		$this->load->model("core/country");
		$this->data['zones'] = $this->model_core_country->getZones(" AND countryid = '230'");
		$this->data['prices'] = array(
										'1000000' => '1000000',
										'2000000' => '2000000',
										'3000000' => '3000000',
										'4000000' => '4000000',
										'5000000' => '5000000',
										'10000000' => '10000000',
										'20000000' => '20000000',
										'50000000' => '50000000',
										'100000000' => '100000000'
										);
		//$keyword = $this->request->get['keyword'];
		//$this->document->breadcrumb .= "Tìm kiếm";
		//$this->data['title'] = "Kết quả tìm kiếm với từ khóa: ".$keyword;
		//$this->getList($keyword);
		//$this->id="content";
		$this->template="addon/searchtour.tpl";
		$this->render();
	}
	
	public function listtour()
	{
		$this->load->model("core/sitemap");
		$this->load->model("core/product");
		$this->load->model("core/country");
		$id = $this->request->get['id'];
		//echo "AAAAAAA";
		$where = " AND zoneid='".$id."' AND countryid='230'";
		$zones = $this->model_core_country->getListZone($where);
		$this->document->breadcrumb .= '<a href="#">'.$zones[0]['zonename'].'</a>';
		$this->data['title'] = $zones[0]['zonename'];
		
		$template = array(
						  'template' => "module/product_list.tpl",
						  'width' => 240,
						  'height' =>159,
						  'paging' => false,
						  'sorting' =>false
						  );
			
		$medias = $this->getProduct($id);
		$arr = array("",32,"",$template,$medias);
		$this->data['productlist'] = $this->loadModule('module/productlist','index',$arr);
		
		$this->id="news";
		$this->template="addon/tour.tpl";
		$this->render();
	}
	
	function getProduct($id)
	{
		$this->load->model('core/sitemap');
		$this->load->model('core/product');
		/*$siteid = $this->member->getSiteId();
		$sitemaps = $this->model_core_sitemap->getListByModule("module/product", $siteid);
		$arrsitemapid = $this->string->matrixToArray($sitemaps,"sitemapid");
		$queryoptions = array();
		$queryoptions['mediaparent'] = '%';
		$queryoptions['mediatype'] = '%';
		$options['refersitemap'] = $arrsitemapid;*/
		$data = $this->model_core_product->getList(" AND (zonefrom='".$id."' OR zoneto='".$id."')");
		
		return $data;
	}
	
	public function selectzoneto()
	{
		$this->load->model("core/country");
		$tourtype = $this->request->get['tourtype'];		
		/*####################################################*/
		$str = "";
		if($tourtype == 'nuocngoai'){
			$country = $this->model_core_country->getCountrys(" AND countryid <> '230'");
			foreach($country as $item){
				$str .= "<optgroup label='".$item['countryname']."'>";
				$zones = $this->model_core_country->getZones(" AND countryid = '".$item['countryid']."'");
				if(count($zones)){
					foreach($zones as $z){
						$str .= "<option value='".$z['zoneid']."'";
						$str .= ">".$z['zonename']."</option>";
					}
				}
                $str .= "</optgroup>";
			}
			$this->data['zonesto'] = $str;
			//$this->data['zonesto'] = $this->model_core_country->getZones(" AND countryid <> '230'");
		}
		else{
			$zones = $this->model_core_country->getZones(" AND countryid = '"."230"."'");
			$str = "";
			if(count($zones)){
				foreach($zones as $z){
					$str .= "<option value='".$z['zoneid']."'";					
					$str .= ">".$z['zonename']."</option>";
				}
			}
			$this->data['zonesto'] = $str;
		}
		$this->data['output'] = $this->data['zonesto'];
		$this->id='post';
		$this->template="common/output.tpl";
		$this->render();
	}
}
?>