<?php
class ControllerAddonBrand extends Controller
{
	private $error = array();
	public function index()
	{
		$template = array(
						  'template' => "module/product_list.tpl",
						  'width' => 120,
						  'height' =>120,
						  'paging' => true,
						  'sorting' =>true
						  );
		$this->getList($template);
		
	}
	
	public function getList($template=array(),$where="",$categoryid="",$sitemapid ="")
	{
		$this->load->model("core/media");
		$this->load->model("core/product");
		$this->load->model("core/sitemap");
		$this->load->helper('image');
		/*if($categoryid=="")
		{
			$arr = split("-",$this->request->get['id']);
			$categoryid = $arr[0];
			$sitemapid = $arr[1];
		}*/
		/*if($sitemapid == "")
			$sitemapid = $this->document->sitemapid;*/
		
		$sitemap = $this->model_core_sitemap->getItem($sitemapid,$this->member->getSiteId());
		if($categoryid)
			$where .= " AND groupkeys like '%[".$categoryid."]%'";
		if($sitemapid)
			$where .= " AND refersitemap like '%[".$sitemapid."]%'";
		
		$order = $_GET['order'];
		$orderby = "";
		switch($order)
		{
			case "az":
				$orderby = " ORDER BY `title` ASC";
				break;
			case "gt":
				$orderby = " ORDER BY `price` ASC";
				break;
			case "gg":
				$orderby = " ORDER BY `price` DESC";
				break;
		}
		
		$medias = $this->model_core_product->getList($where);
		
		
		$this->data['medias'] = array();
			
		
			$index = -1;
			$page = $_GET['page'];
			
			$x=$page;		
			$limit = 16;
			$total = count($medias);
			//$uri = $this->document->createLink($sitemapid);
			$uri = $this->document->getURI();
			// work out the pager values 
			$this->data['pager']  = $this->pager->pageLayoutWeb($total, $limit, $page,$uri); 
			
			$pager  = $this->pager->getPagerData($total, $limit, $page); 
			$offset = $pager->offset; 
			$limit  = $pager->limit; 
			$page   = $pager->page;
			for($i=$offset;$i < $offset + $limit && count($medias[$i])>0;$i++)
			{
				$index += 1;
				
				$media = $medias[$i];
				
				$arr = $this->string->referSiteMapToArray($media['refersitemap']);
				$sitemapid = $arr[0];
				$link = $this->document->createLink($sitemapid,$media['alias']);
				$imagethumbnailpng = "";
				$imagethumbnail = "";
				//if($media['imagepath'] != ""  )
				{
					$imagethumbnailpng = HelperImage::resizePNG($media['imagepath'], $template['width'], $template['height']);
					@$imagethumbnail = HelperImage::fixsize($media['imagepath'], $template['width'], $template['height']);
				}
	
				$properties= $this->string->referSiteMapToArray($media['groupkeys']);
				
				$saleoff = round(($media['priceold']-$media['price'])/$media['priceold']*100, 0);
				$this->data['medias'][] = array(
					'id' => $media['id'],
					'productname' => $media['productname'],
					'description' => html_entity_decode($media['description']),
					'imagethumbnailpng' => $imagethumbnailpng,
					'priceold' => $media['priceold'],
					'price' => $media['price'],
					'saleoff' => $saleoff,
					'startdate' => $media['startdate'],
					'post' => $media['post'],
					'summary' => $media['summary'],
					'warranty' => $media['warranty'],
					'alias' => $media['alias'],
					'sale' => $media['sale'],
					'soldout' => $media['soldout'],
					'imagethumbnail' => $imagethumbnail,
					'link' => $link,
					'properties'=>$properties
				);
				
				
				//print_r($this->data['medias']);
		
			
		$this->id="content";
		$this->template=$template['template'];
		$this->render();	
	}
}
	
}
?>