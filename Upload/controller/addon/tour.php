<?php
class ControllerAddonTour extends Controller
{
	public function index()
	{
		$this->load->model("core/sitemap");
		$this->load->model("core/product");
		$this->load->model("core/country");
		$id = $this->request->get['id'];
		
		if($id == 'search'){
			$this->document->breadcrumb .= '<a href="#">'.'Tìm tour du lịch'.'</a>';
			$this->data['title'] = "Tìm tour du lịch";
			
			$zonefrom = $_GET['zonefrom'];
			$zoneto = $_GET['zoneto'];
			$pricefrom = $_GET['pricefrom'];
			$priceto = $_GET['priceto'];
			$startdate = $_GET['startdate'];
			$tourtype = $_GET['tourtype'];
			
			$where = " AND tourtype = '".$tourtype."'";
			if(trim($zonefrom) != ''){
				$where .= " AND zonefrom = '".$zonefrom."'";
			}
			if(trim($zoneto) != ''){
				$where .= " AND zoneto = '".$zoneto."'";
			}
			if(trim($pricefrom) != ''){
				$where .= " AND price >= '".$pricefrom."'";
			}
			if(trim($priceto) != ''){
				$where .= " AND price <= '".$priceto."'";
			}
			if(trim($startdate) != ''){
				$where .= " AND startdate >= '".$startdate."'";
			}
		}
		elseif($id == 'trongnuoc'){
			$this->document->breadcrumb .= '<a href="#">'.'Tour trong nước'.'</a>';
			$this->data['title'] = "Tour trong nước";
			$where = " AND tourtype = '".$id."'";
			$this->data['flag']	= 'tourtype';	
		}
		elseif($id == 'nuocngoai'){
			$this->document->breadcrumb .= '<a href="#">'.'Tour nước ngoài'.'</a>';
			$this->data['title'] = "Tour nước ngoài";
			$where = " AND tourtype = '".$id."'";	
			$this->data['flag']	= 'tourtype';	
		}
		else{
			$where = " AND zoneid='".$id."' AND countryid='230'";
			$zones = $this->model_core_country->getListZone($where);
			$this->document->breadcrumb .= '<a href="#">'.$zones[0]['zonename'].'</a>';
			$this->data['title'] = $zones[0]['zonename'];
			
			$where = " AND (zonefrom='".$id."' OR zoneto='".$id."')";
		}
		
		$medias = $this->getProduct($where);
		$template = array(
						  'template' => "module/product_list.tpl",
						  'width' => 240,
						  'height' =>159,
						  'paging' => false,
						  'sorting' =>false
						  );
			
		$arr = array("",32,"",$template,$medias);
		$this->data['productlist'] = $this->loadModule('module/productlist','index',$arr);
		
		$this->id="news";
		$this->template="addon/tour.tpl";
		$this->render();
	}
	
	function getProduct($where)
	{
		$this->load->model('core/sitemap');
		$this->load->model('core/product');
		/*$siteid = $this->member->getSiteId();
		$sitemaps = $this->model_core_sitemap->getListByModule("module/product", $siteid);
		$arrsitemapid = $this->string->matrixToArray($sitemaps,"sitemapid");
		$queryoptions = array();
		$queryoptions['mediaparent'] = '%';
		$queryoptions['mediatype'] = '%';
		$options['refersitemap'] = $arrsitemapid;*/
		$data = $this->model_core_product->getList($where);
		
		return $data;
	}
}
?>