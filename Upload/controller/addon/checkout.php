<?php
class ControllerAddonCheckout extends Controller
{
	private $error = array();
	public function index()
	{
		$this->document->breadcrumb .= '<a href="#">'.$this->data["text_orderinformation"].'</a>';
		$this->data['title'] = $this->data['text_orderinformation'];
		$this->getList();
		$this->getMemberInfor();
		$template = array(
						  'template' => "home/infor.tpl"
						  );
		$siteid = $this->user->getSiteId();
		$mediaid = $siteid."huong-dan-mua-hang";
		$arr = array($mediaid,$template);
		$this->data['infor'] = $this->loadModule('module/block','showContent',$arr);
		
		$this->id="content";
		$this->template="addon/checkout.tpl";
		$this->render();
	}
	
	public function getList()
	{
		$this->data['medias'] =array();

		if(isset($_SESSION['cart']))
		{
			$this->data['medias'] = $_SESSION['cart'];
		}
		
	}
	
	private function getMemberInfor()
	{
		$this->load->model("core/user");
		$this->data['member'] = $this->model_core_user->getItem($this->member->getId());
		if(count($this->data['member']))
			$this->data['readonly'] = 'readonly="readonly"';
	}
	
	public function checkout()
	{
		$data = $this->request->post;
				
		if($this->validateForm($data))
		{
			$this->load->model("addon/order");
			$this->load->model("core/product");
			//Luu thong tin don hang
			$orderid = $this->model_addon_order->insert($data);
			$sanpham = "";
			//Luu chi tiet don hang
			$count = 1;
			
			foreach($_SESSION['cart'] as $item)
			{
				$detail['orderid'] = $orderid;
				$detail['mediaid'] = $item['mediaid'];
				$detail['quantity'] = $item['qty'];
				$detail['price'] = $item['price'];
				$detail['discount'] = 0;
				
				$sanpham .= "=============".$count."============="."<br />";
				$sanpham .= "<b>Tên sản phẩm:</b>".$item['title']."<br />";
				$sanpham .= "<b>Giá thành:</b>".$item['price']."<br />";
				$sanpham .= "<b>Số lượng:</b>".$item['qty']."<br />";
				
				$count++;
				
				$this->model_addon_order->saveOrderProduct($detail);
				/*Luot mua*/
				$tmp_media = $this->model_core_product->getItem($item['mediaid']);
				$amount = $tmp_media['soldout'] + $item['qty'];
				$this->model_core_product->updateCol($item['mediaid'], 'soldout', $amount);
			}
			$this->load->model("core/media");
			$email = $this->model_core_media->getInformation("setting", 'EmailContact');
			$email1 = $email;
			$arrmail = array();
			if($email1)
				$arrmail[] = $email1;
			if($email2)
				$arrmail[] = $email2;
			if($email3)
				$arrmail[] = $email3;
			$payment = "";
			$payment = "<b>Họ tên khách hàng:</b>".$data['customername']."<br />";
			$payment .= "<b>Email:</b>".$data['email']."<br />";
			$payment .= "<b>Địa chỉ:</b>".$data['address']."<br />";
			$payment .= "<b>Điện thoại:</b>".$data['phone']."<br />";
			$payment .= "<b>Ghi chú:</b>".$data['comment']."<br />";
			
			
			$mail['from'] = 'sales@yotab.vn';
			$mail['FromName'] = 'Yotab';
			$mail['to'] = 'hoc.do@bensolution.com';
			$mail['name'] = "sales@yotab.vn";
			$mail['cc'] = $this->document->mailcc;
			$mail['subject'] =  '[Yotab.vn]THONG BAO DAT HANG THANH CONG';
			$mail['body'] = $payment;
			$mail['body'] .= "<br>".$sanpham;
			$this->mailsmtp->sendMail($mail);
			
			
			unset($_SESSION['cart']);
			$this->data['output'] = "true-".$orderid;
		}
		else
		{
			foreach($this->error as $item)
			{
				$this->data['output'] .= $item."<br>";
			}
		}
		$this->id='content';
		$this->template='common/output.tpl';
		$this->render();
	}
	
	private function validateForm($data)
	{
		
		if(trim($data['customername']) =="")
		{
      		$this->error['customername'] = $this->data['war_fullnamenotnull'];
    	}
		
		if ($data['email'] == "") 
		{
      		$this->error['email'] = $this->data['war_emailnotnull'];
    	}
		else
		{
			if(!$this->validation->_checkEmail($data['email']))
			{
				$this->error['email'] = $this->data['war_emailnotformate'];
			}
		}

		
		
		if (count($this->error)==0) {
	  		return TRUE;
		} else {
	  		return FALSE;
		}
	}
}
?>