<?php
class ControllerAddonSaleoff extends Controller
{
	private $error = array();
	public function index()
	{
		$this->data['saleoff'] =  $this->model_core_media->getItem($this->member->getSiteId()."saleoffcontent");
		
		
		$this->getList();
		
		$this->id="content";
		$this->template="addon/saleoff.tpl";
		$this->render();
	}
	
	public function getList()
	{
		$this->load->model("core/product");
		$this->load->model("core/saleoff");
		$this->load->helper('image');
		$alias = $this->request->get['id'];
		if($alias == '')
		{
			$this->data['media'] = $this->model_core_product->getItem('13');
			$parentproduct = '13';
		}
		else
		{
			$this->data['media'] = $this->model_core_product->getByAlias($alias);
			$parentproduct = $this->data['media']['id'];
		}
		
		$imagethumbnail = "";
		if($this->data['media']['imagepath'] != "")
		{
			$imagethumbnailpng = HelperImage::resizePNG($this->data['media']['imagepath'], $template['width'], $template['height']);
			@$imagethumbnail = HelperImage::resizePNG($this->data['media']['imagepath'], 386, 285);
		}
		
		$this->data['media']['imagethumbnail'] = $imagethumbnail;
		
		$arrChild = $this->model_core_product->getListByParent($this->data['media']['id'],"ORDER BY position ASC");
		
		$updateChild = array();
		$count = 0;
		foreach($arrChild as $key => $item){
			if($item['purchasenumber'] < $item['salenumber'])
			{
				$this->model_core_saleoff->updateCol($item['id'],"soldout",'true');
				unset($arrChild[$count]);
				break;
			}
			$count ++;
		}
		
		if(count($arrChild) > 0)	
		{
			foreach($arrChild as $item){
				$this->model_core_saleoff->updateCol($item['id'],"soldout",'false');
			}
		}
		
		$arrChild = $this->model_core_product->getListByParent($this->data['media']['id'],"ORDER BY position ASC");
				
		$this->data['child'] = $arrChild;
	
		
		//user order
		$where = " AND parentproduct = '".$parentproduct."' ORDER BY purchasedate DESC";
		$medias = $this->model_core_saleoff->getList($where);

//Page
		$page = isset ( $_GET["page"] ) ? intval ( $_GET["page"] ) : 1;

		$limit = 10;
		
		$page_start = ( $page - 1 ) * $limit;
		$page_end = $page * $limit;
		
		$numberpage = ceil (count($medias) / $limit ); 
		
		//in ra tổng số trang
		if ( $numberpage > 1 )
		{
			$list_page = " <b> Trang: </b>";
		
			for ( $i = 1; $i <= $numberpage; $i++ )
			{
				if ( $i == $page )
				{
					$list_page .= " <td>[ <b>{$i}</b> ]</td> ";
				}
				else
				{
					$list_page .= "<td><a href='saleoff.html?page={$i}'> {$i} </a></td>";
				}
			}
		}
		
		$i = 0;
		
		if(count($medias) > 0)
		{
			foreach($medias as $key => $item)
			{
			
				if($i >= $page_start)
				{
					$this->data['listuser'][] = $item;
					
				}
				
				$i++;
				
				if ($i >= $page_end)
				{
					break;
				}
			}
		}
		
		$this->data['listpage'] = $list_page;
		
	}
	
	public function productRegister()
	{
		
		$this->data['id'] = $this->request->get['id'];
				
		$this->id="news";
		$this->template="addon/register_form.tpl";
		$this->render();
	}
	
	public function orderRegister()
	{
		$this->load->model("core/product");
		$this->load->model("core/saleoff");
		$data = $this->request->post;

		if($this->validateForm($data))
		{
			$product = $this->model_core_product->getItem($data['productid']);
		
			$purchasenumber = $product['purchasenumber'] + 1;
			
			$this->model_core_saleoff->updateCol($product['id'],"purchasenumber",$purchasenumber);
			
			
			$data['productid'] = $product['id'];
			$data['parentproduct'] = $product['productparent'];
			$data['purchasedate'] = $this->date->getToday();
			$data['purchaseprice'] = $product['price'];
			
			$this->model_core_saleoff->insert($data);
						
			$this->data['output'] = "true";
		}
		else
		{
			foreach($this->error as $item)
			{
				$this->data['output'] .= $item."<br>";
			}
		}
		
		
		//$this->data['output'] = "true";
		$this->template="common/output.tpl";
		$this->render();
	}
	
	private function validateForm($data)
	{
		if(trim($data['customername']) == "")
		{
      		$this->error['customername'] = "Bạn chưa nhập họ tên";
    	}
		
		if ($data['email'] == "") 
		{
      		$this->error['email'] = "Bạn chưa nhập email";
    	}
		else
		{
			if(!$this->validation->_checkEmail($data['email']))
			{
				$this->error['email'] = "Email không đúng dịnh dạng";
			}
			
		}
		
		if(trim($data['phone']) =="")
		{
      		$this->error['phone'] = "Bạn chưa nhập số điện thoại liên lạc";
    	}
		
		if(trim($data['idcard']) =="")
		{
      		$this->error['idcard'] = "Bạn chưa nhập CMND";
    	}
		
		if(trim($data['address']) =="")
		{
      		$this->error['address'] = "Bạn chưa nhập địa chỉ liên hệ";
    	}
		
		
		if (count($this->error)==0) {
	  		return TRUE;
		} else {
	  		return FALSE;
		}
	}
	
}

?>