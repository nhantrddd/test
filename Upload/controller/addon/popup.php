<?php
class ControllerAddonPopup extends Controller
{
    public function index()
    {
        /*
         * Load resource
         */
        $this->load->model("core/media");
        
        /*
         * Get media id
         */
        $mediaId = "popup";
        
        /*
         * Get detail
         */
        $attr                   = $this->model_core_media->getInformation($mediaId, "attr");
        $attr                   = (trim($attr) != "") ? (array)json_decode($attr) : false;
        $attr                   = ($attr !== false && isset($attr["status"]) && $attr["status"] == 1) ? $attr : false;
        $detail                 = $this->model_core_media->getInformation($mediaId, "detail");
        $this->data["popup"]    = array("attr"=>$attr, "detail"=>$detail);
        
        /*
         * Set view
         */
        $this->id       = "content";
        $this->template = "addon/popup.tpl";
        $this->render();
    }
}
?>