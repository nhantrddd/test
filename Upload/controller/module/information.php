<?php
class ControllerModuleInformation extends Controller
{
	/*function __construct() 
	{
		$this->iscache = true;
	 	$this->name ="Information".$this->document->sitemapid;
   	}*/
	public function index($sitemapid="")
	{
		$this->load->model("core/media");
		$this->load->model("core/sitemap");
		if($sitemapid=="")
			$sitemapid = $this->document->sitemapid;
		
		$this->data['post'] = $this->model_core_media->getItem($this->member->getSiteId().$sitemapid);
		$this->document->title .= " - ".$this->data['post']['title'];
		if(count($this->data['post']) == 0)
		{
			$this->data['post']['description'] = "Updating...";
		}
		
		$this->data['post']['description'] = html_entity_decode($this->data['post']['description']);
		
		/*$arrChild = $this->model_core_sitemap->getListByParent('hoidap',$this->member->getSiteId());
		echo "<pre>";
		print_r($arrChild);
		echo "</pre>";
		foreach($arrChild as $item)
		{
			$arrSitemap[] = $item[];
		}*/
		/*foreach($arrChild as $item)
		{
			
		}*/
		
	/*	$arrChildSitemapid = array();
		$arrChildSitemapid = $this->string->matrixToArray($arrChild,"sitemapid");
		$arrChildSitemapid[] = 'hoidap';
		echo "<pre>";
		print_r($arrChildSitemapid);
		echo "<pre>";*/
		//echo "<pre>";
		
		/*$flag = 'false';
		foreach($arrChildSitemapid as $sitemap)
		{
			if($sitemap == '$sitemapid')
			{
				$flag = 'true';	
			}
		}*/
		
		$str = array();
		$this->getArraySitemap("hoidap", &$str);
		//$str = $str.'hoidap';
		//echo $str;
		//$arrchild = 
		
		$str[] = 'hoidap';
		
		foreach($str as $item)
		{
			if($sitemapid == $item)
			{
				$flag = "true";	
			}	
		}
		
		$this->data['menuhoidap'] = $this->getMenu("hoidap","Active");
		
		$this->id="information";
		if($flag == 'true')
		{
			$this->template="module/hoidap.tpl";
		}else{
			$this->template="module/information.tpl";	
		}
		$this->render();
	}
	
	public function getArraySitemap($parentid, &$str)
	{
		$status = "Active";
		$siteid = $this->user->getSiteId();	
		
		$arrChild = $this->model_core_sitemap->getListByParent($parentid,$siteid,$status);
		if (count($arrChild) == 0)
		{
			return;
		}
		
		foreach($arrChild as $item)
		{
			//$str .= $item['sitemapid'].",";
			$str[] = $item['sitemapid'];
			$this->getArraySitemap($item['sitemapid'], &$str);
		}
		
	}
	
	
	public function getMenu($parentid,$status)
	{
		$siteid = $this->user->getSiteId();
		$str = "";
		
		$sitemaps = $this->model_core_sitemap->getListByParent($parentid, $siteid, $status);
		
		foreach($sitemaps as $item)
		{
			$childs = $this->model_core_sitemap->getListByParent($item['sitemapid'], $siteid, $status);
			
			$link = "<a class='left'>".$item['sitemapname']."</a>";
			
			if(substr($item['moduleid'],0,6) == "group/")
			{
				$item['moduleid'] = "module/information";
			}
			
			
			if($item['moduleid'] != "group" && $item['moduleid'] != "homepage")
			{
				//$link='<a class="left" href="?route='.$item['moduleid']."&sitemapid=".$item['sitemapid'].'" title="[Detail]">'.$item['sitemapname'].'</a>';
				$link = "<a  href='".$this->document->createLink($item['sitemapid'])."'>".$item['sitemapname']."</a>";
			}
			
			$str .= "<li>";
			$str .= "<div>";
			$str .= $link;
			
			if(count($childs) > 0)
			{
				$str .= "<span></span>";
				$str .= '<div class="clearer">&nbsp;</div>';
				$str .= "</div>";
				
				$str .= "<ul id='".$item['sitemapid']."'>";
				$str .= $this->getMenu($item['sitemapid'],$status);
				$str .= "</ul>";
			}
			else
			{
				$str .= '<div class="clearer">^&nbsp;</div>';
				$str .= "</div>";
				
			}
			$str .= "</li>";
		}
		
		return $str;
		
	}
}
?>