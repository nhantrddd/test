<?php
class ControllerModuleProduct extends Controller
{
	public function index()
	{
		$this->load->model("core/sitemap");
		$this->load->model("core/product");
		
		//print_r($this->data['category']);
		$sitemapid = $this->document->sitemapid;
		$siteid = $this->member->getSiteId();
		
		//load banner
		$this->data['banner'] = $this->loadModule('common/banner');
		$search=$this->request->get['search'];
		
		//lấy giá trị cha của sitemap con
		$sitemapdetail = $this->model_core_sitemap->getRoot($this->document->sitemapid, $siteid);
		$bannerdetail = "banner".$sitemapdetail;

		$template = array(
						  'template' => "addon/bannerdetail.tpl",
						  'width' => 0,
						  'height' =>0
					);
	
		$arr = array($bannerdetail,0,"",$template);
		//truyền tham số module và load lên giao diện
		$this->data['bannerdetail'] = $this->loadModule('module/block','getList',$arr);	
		
		$this->data['sitemap'] = $this->model_core_sitemap->getItem($sitemapid, $siteid);
		$para = $this->document->getPara();
		if(count($para)){
			foreach($para as $key => $val)
			{
				$para[$key]	= urldecode($val);
			}
		}
	
		$template = array(
						  'template' => "module/product_list.tpl",
						  'width' => 240,
						  'height' =>159,
						  'paging' => true,
						  'sorting' =>true
						  );
		
		if($search!="true")
		{
			$arr = array($this->document->sitemapid,24,"",$template);
			$this->data['listproduct'] = $this->loadModule('module/product','getList',$arr);
		}
		else
		{
			$where = "";
			if(trim($para['productname']))
			{
				$where .= " AND productname like '%".$para['productname']."%'";
			}
			if(trim($para['manufacturer']))
			{
				$where .= " AND manufacturerid = '".$para['manufacturer']."'";
			}
			if(trim($para['sitemapid']))
			{
				$child = array();

				$this->model_core_sitemap->getTreeSitemap($para['sitemapid'],$child,$this->member->getSiteId());
				$listsitemap = array();
				if(count($child))
				{
					foreach($child as $item)
						$listsitemap[] = $item['sitemapid'];
				}
				$arr = array();
				foreach($listsitemap as $sitemapid)
				{
					$arr[] = " refersitemap like '%[".$sitemapid."]%'";
					
				}
				$where .= " AND (". implode($arr," OR ").")";
			}
			if($para['giatu'])
			{
				$where .= " AND price >='".$para['giatu']."'";	
			}
			
			if($para['giaden'])
			{
				$where .= " AND price <='".$para['giaden']."'";	
			}
			
			$data_product = $this->model_core_product->getList($where);
			if(count($data_product))
			{
				$arr = array('',5,"",$template,$data_product);
				$this->data['listproduct'] = $this->loadModule('module/product','getList',$arr);
			}
			else
			{
				$this->data['listproduct'] = "Không có sản phẩm nào phù hợp với tiêu chí tìm kiếm của bạn!";
			}
		}
		
		//
		//$this->data['frmsearch'] = $this->loadModule('sitebar/frmsearch');
		//Dien thoai duoc mua nhieu
		/*$template = array(
						  'template' => "sitebar/sanphamduocmuanhieu.tpl",
						  'width' => 80,
						  'height' =>96
						  );
			
		$arr = array("sanphambanchay","dienthoai",10,"",$template);
		$this->data['sanphambanchay'] = $this->loadModule('module/product','getListStyle',$arr);*/
		//Phu kien duoc mua nhieu
		/*$template = array(
						  'template' => "sitebar/phukienduocmuanhieu.tpl",
						  'width' => 80,
						  'height' =>96
						  );
			
		$arr = array("sanphambanchay","phukien",10,"",$template);
		$this->data['phukienbanchay'] = $this->loadModule('module/product','getListStyle',$arr);*/
		
		$this->id="news";
		$this->template="module/product.tpl";
		$this->render();
	}
	
	public function getList($sitemapid="", $count = 0,$headername = "", $template = array(),$medias=array())
	{
		
		$this->load->model("core/sitemap");
		$this->load->model("core/product");
		$this->load->helper('image');
		
		if($sitemapid == "")
			$sitemapid = $this->document->sitemapid;
		
		$siteid = $this->member->getSiteId();
		
		$this->data['sitemap'] = $this->model_core_sitemap->getItem($sitemapid, $siteid);
		
		//$this->document->title .= " - ".$this->data['sitemap']['sitemapname'];
		$step = (int)$this->request->get['step'];
		$to = $count;
		
		//Get list
/*		$queryoptions = array();
		$queryoptions['mediaparent'] = '%';
		$queryoptions['mediatype'] = '%';
		$queryoptions['refersitemap'] = $sitemapid;*/
		//Get list
		$child = array();

		$this->model_core_sitemap->getTreeSitemap($sitemapid,$child,$this->member->getSiteId());
		
		$listsitemap = array();
		if(count($child))
		{
			foreach($child as $item)
				$listsitemap[] = $item['sitemapid'];
		}
		$arr = array();
		foreach($listsitemap as $sitemapid)
		{
			$arr[] = " refersitemap like '%[".$sitemapid."]%'";
			
		}
		$where = "AND (". implode($arr," OR ").")";
		if($mediaid == "")
		{
			if(count($medias)==0)
				$medias = $this->model_core_product->getList($where);

			
			
			$this->data['medias'] = array();
			
		
			$index = -1;
			$page = $_GET['page'];
			
			$x=$page;		
			$limit = $count;
			$total = count($medias);
			//$uri = $this->document->createLink($sitemapid);
			$uri = $this->document->getURI();
			// work out the pager values 
			$this->data['pager']  = $this->pager->pageLayoutWeb($total, $limit, $page,$uri); 
			
			$pager  = $this->pager->getPagerData($total, $limit, $page); 
			$offset = $pager->offset; 
			$limit  = $pager->limit; 
			$page   = $pager->page;
			for($i=$offset;$i < $offset + $limit && count($medias[$i])>0;$i++)
			{
				$index += 1;
				
				$media = $medias[$i];
				
				$arr = $this->string->referSiteMapToArray($media['refersitemap']);
				$sitemapid = $arr[0];
				$link = $this->document->createLink($sitemapid,$media['alias']);
				$imagethumbnailpng = "";
				$imagethumbnail = "";
				//if($media['imagepath'] != ""  )
				{
					$imagethumbnailpng = HelperImage::resizePNG($media['imagepath'], $template['width'], $template['height']);
					@$imagethumbnail = HelperImage::fixsize($media['imagepath'], $template['width'], $template['height']);
				}
	
				$properties= $this->string->referSiteMapToArray($media['groupkeys']);
				$saleoff = round(($media['priceold']-$media['price'])/$media['priceold']*100, 0);
				$this->data['medias'][] = array(
					'mediaid' => $media['id'],
					'title' => $media['productname'],
					'summary' => $media['summary'],
					'priceold' => $media['priceold'],
					'price' => $media['price'],
					'saleoff' => $saleoff,
					'startdate' => $media['startdate'],
					'properties' => $properties,
					'imagethumbnail' => $imagethumbnail,
					'imagetpreview' => $imagetpreview,
					'fileid' => $media['imageid'],
					'soldout' => $media['soldout'],
					'statusdate' => $this->date->formatMySQLDate($media['updatedate'], 'longdate', "/"),
					'link' => $link
				);
				
			
				if($count>0)
					if($index >= $count)
						break;
			}
		}
		
		$this->id="news";
		$this->template=$template['template'];
		$this->render();
	
	}
	
	
	public function getDetail($alias="", $template = array())
	{
		$this->load->model("core/media");
		//$this->load->model("core/category");
		$this->load->model("core/sitemap");
		$this->load->model("core/product");
		$this->load->helper('image');
		//$this->data['category'] = $this->loadModule('common/category');
		
		if($sitemapid == "")
			$sitemapid = $this->document->sitemapid;
		$siteid = $this->member->getSiteId();
		$this->data['sitemap'] = $this->model_core_sitemap->getItem($sitemapid, $siteid);
		$this->document->title .= " - ".$this->data['sitemap']['sitemapname'];
		$product=$this->model_core_product->getByAlias($alias);
		$this->data['tuvanmuahang'] = $this->model_core_media->getItem($this->member->getSiteId()."tuvanmuahang");
		$this->document->title .= " - ".$product['productname'];
		//print_r($this->data['tuvanmuahang']['description'] );
		$subimage[] = $product['imageid'];
		$subimage = array_merge($subimage, explode(",",$product['subimageid']));
		
		foreach($subimage as  $key=>$item)
		{
			$file = $this->model_core_file->getFile($item);
			if($this->string->isImage($file['extension']))
			{
				$this->data['subimage'][$key] = $file;
				$this->data['subimage'][$key]['imageicon'] = HelperImage::fixsize($file['filepath'], $template['widthicon'], $template['heighticon']);
				$this->data['subimage'][$key]['imagethumbnail'] = HelperImage::fixsize($file['filepath'], $template['width'], $template['height']);
				$this->data['subimage'][$key]['imagepreview'] = HelperImage::resizePNG($file['filepath'],  $template['widthpreview'], $template['heightpreview']);
				
			}	
		}
		

		$imagethumbnail = "";				
		{
			$imagethumbnailpng = HelperImage::resizePNG($product['imagepath'], $template['width'], $template['height']);
			$imagepreview = HelperImage::resizePNG($product['imagepath'], $template['widthpreview'], $template['heightpreview']);
			@$imagethumbnail = HelperImage::fixsize($product['imagepath'], $template['width'], $template['height']);
			
		}
		
		$this->data['properties'] = $this->string->referSiteMapToArray($product['groupkeys']);
		$saleoff = round(($product['priceold']-$product['price'])/$product['priceold']*100, 0);
		$this->data['media']= array(
					'id' => $product['id'],
					'productname' => $product['productname'],
					'description' => html_entity_decode($product['description']),
					'imagethumbnailpng' => $imagethumbnailpng,
					'priceold' => $product['priceold'],
					'price' => $product['price'],
					'saleoff' => $saleoff,
					'startdate' => $product['startdate'],
					'post' => $product['post'],
					'summary' => $product['summary'],
					'manufacturerid' => $product['manufacturerid'],
					'manufacturername' => $product['manufacturername'],
					'warranty' => $product['warranty'],
					'sale' => $product['sale'],
					'sitemapid' => $sitemapid,
					'imagethumbnail' => $imagethumbnail,
					'imagepreview' => $imagepreview
				);
		
		$this->load->model('core/comment');

		$where=" AND mediaid='".$alias."'";
		$this->data['comments']=$this->model_core_comment->getList($where,0,10);
		
		
		//echo $product['id'];
		//echo $sitemapid;
		/*
		$this->data['status'] = array();
		$this->model_core_category->getTree("status",$this->data['status']);
		unset($this->data['status'][0]);
		*/
		
		/*
		$statusid = "";
		foreach($this->data['status'] as $status)
		{
			if(in_array($status['categoryid'],$this->data['properties']))
				$statusid = $status['categoryid'];
		}
		*/
		
		//print_r($this->data['status']);
		
		$mediaid = $this->request->get['id'];
		
		$where = " AND alias not like '".$mediaid."' AND refersitemap like '%".$sitemapid."%'";
		$where .= " AND (zonefrom = '".$product['zonefrom']."' OR zoneto ='".$product['zoneto']."')";
		$templatesanphamcungloai = array(
							  'template' => "module/sanphamcungloai.tpl",
							  'width' => 235,
							  'height' =>159
							  
							  );
		
		$arr = array($templatesanphamcungloai,$where);
		$this->data['saphamcungloai'] = $this->loadModule('addon/brand','getList',$arr);
		
		//$mediaid = $this->request->get['id'];
		//echo $mediaid = $this->request->get['id'];
		
		//print_r($this->data['saphamcungloai']);
		
		
		
		$this->id="news";
		$this->template=$template['template'];
		$this->render();
	
	}	

	public function getListStyle($gruopkeys="",$sitemapid="", $count = 0,$headername = "", $template = array(),$medias=array())
	{
		//$this->load->model("core/media");
		$this->load->model("core/sitemap");
		$this->load->model("core/product");
		$this->load->helper('image');
		
		if($sitemapid == "")
			$sitemapid = $this->document->sitemapid;
		
		$siteid = $this->member->getSiteId();
		
		$this->data['sitemap'] = $this->model_core_sitemap->getItem($sitemapid, $siteid);
		
		//$this->document->title .= " - ".$this->data['sitemap']['sitemapname'];
		$step = (int)$this->request->get['step'];
		$to = $count;
		
		//Get list
/*		$queryoptions = array();
		$queryoptions['mediaparent'] = '%';
		$queryoptions['mediatype'] = '%';
		$queryoptions['refersitemap'] = $sitemapid;*/
		
		if($mediaid == "")
		{
			if(count($medias)==0)
				//$medias = $this->model_core_media->getPaginationList($queryoptions, $step, $to);
				$medias = $this->model_core_product->getListStyles($gruopkeys,$sitemapid, $to);

			
			
			$this->data['medias'] = array();
			
		
			$index = 0;
			foreach($medias as $media)
			{
				$index += 1;
				$arr = $this->string->referSiteMapToArray($media['refersitemap']);
				$sitemapid = $arr[0];
				$link = $this->document->createLink($sitemapid,$media['alias']);
				$imagethumbnailpng = "";
				$imagethumbnail = "";
				//if($media['imagepath'] != ""  )
				{
					$imagethumbnailpng = HelperImage::resizePNG($media['imagepath'], $template['width'], $template['height']);
					@$imagethumbnail = HelperImage::fixsize($media['imagepath'], $template['width'], $template['height']);
				}
				$properties= $this->string->referSiteMapToArray($media['groupkeys']);
				
				$this->data['medias'][] = array(
					'id' => $media['id'],
					'productname' => $media['productname'],
					'description' => html_entity_decode( $media['description']),
					'imagethumbnailpng' => $imagethumbnailpng,
					'price' => $media['price'],
					'imagethumbnail' => $imagethumbnail,
					'link' => $link,
					'priceold' => $media['priceold'],
					'properties'=>$properties
				);
				
				
				
			
				if($count>0)
					if($index >= $count)
						break;
			}
			
			
			
			
			
		}
		
		$this->id="news";
		$this->template=$template['template'];
		$this->render();
	
	}
	
	
	
	
	
	
	public function getLink($sitemapid="", $count = 5,$headername = "", $template = array(),$medias=array())
	{
		$this->load->model("core/media");
		$this->load->model("core/sitemap");
		$this->load->helper('image');
		if($sitemapid == "")
			$sitemapid = $this->document->sitemapid;
		$siteid = $this->member->getSiteId();
		$this->data['sitemap'] = $this->model_core_sitemap->getItem($sitemapid, $siteid);
		//$this->document->title .= " - ".$this->data['sitemap']['sitemapname'];
		$step = (int)$this->request->get['step'];
		$to = $count;
		
		//Get list
		$queryoptions = array();
		$queryoptions['mediaparent'] = '%';
		$queryoptions['mediatype'] = '%';
		$queryoptions['refersitemap'] = $sitemapid;
		
		if($mediaid == "")
		{
			$medias = $this->model_core_media->getPaginationList($queryoptions, $step, $to);
			
			if(count($medias) == 1)
			{
				
			}
			
			$this->data['medias'] = array();
			
		
			$index = -1;
			foreach($medias as $media)
			{
				$index += 1;
				
				$link = $this->model_core_media->getInformation($media['mediaid'],"Link");
				
				$imagethumbnail = "";
				if($media['imagepath'] != ""  )
				{
					$imagethumbnailpng = HelperImage::resizePNG($media['imagepath'], $template['width'], $template['height']);
					@$imagethumbnail = HelperImage::fixsize($media['imagepath'], $template['width'], $template['height']);
					
				}
	
				
				$this->data['medias'][] = array(
					'mediaid' => $media['mediaid'],
					'title' => $media['title'],
					'summary' => $media['summary'],
					'imagethumbnail' => $imagethumbnail,
					'statusdate' => $this->date->formatMySQLDate($media['statusdate'], 'longdate', "/"),
					'link' => $link
				);
				
			}
			
			
		}
		
		$this->id="news";
		$this->template=$template['template'];
		$this->render();
	
	}
	
	public function getSitemaps($sitemapid="",$count = 0,$headername = "", $template = array())
	{
		$this->load->model('core/sitemap');	
		$siteid = $this->member->getSiteId();
		$this->data['sitemap'] = $this->model_core_sitemap->getItem($sitemapid, $siteid);
		$this->data['list']=$this->getMenu($sitemapid);
		$this->id="sitemap";
		$this->template="sitebar/catalogue.tpl";
		$this->render();
	}
	
	public function getMenu($parentid)
	{
		
		
		$siteid = $this->member->getSiteId();
		
		$rootid = $this->model_core_sitemap->getRoot($this->document->sitemapid, $siteid);

		if($this->document->sitemapid == "")
			$rootid = 'homepage';
		$str = "";
		
		
		$sitemaps = $this->model_core_sitemap->getListByParent($parentid, $siteid);
		
		foreach($sitemaps as $item)
		{
			$childs = $this->model_core_sitemap->getListByParent($item['sitemapid'], $siteid);
			
			$currenttab = "";
			if($item['sitemapid'] == $rootid) 
				$currenttab = "class='current-tab'";
			
			$link = "<a ".$currenttab.">".$item['sitemapname']."</a>";
			
			if($item['moduleid'] != "group")
			{
				$link = "<a ".$currenttab." href='".$this->document->createLink($item['sitemapid'])."'>".$item['sitemapname']."</a>";
			}
			if($item['moduleid'] == "homepage"){
				$link = "<a ".$currenttab." href='index.php'>".$item['sitemapname']."</a>";
			}
			
			$str .= "<li>";
			$str .= $link;
			
			if(count($childs) > 0)
			{
				$str .= "<ul>";
				$str .= $this->getMenu($item['sitemapid']);
				$str .= "</ul>";
			}

			$str .= "</li>";
		}
		
		//return $str;
		$this->data['menu']=$str;
		$this->id="content";
		$this->template="sitebar/listchild.tpl";
		$this->render();
		
	}
	
	public function getMenuChild($parentid,$headername = "", $template = array())
	{
		$this->load->model('core/sitemap');	
		$this->load->model('core/media');	
		$this->load->helper('image');
		$siteid = $this->member->getSiteId();
		$sitemaps = $this->model_core_sitemap->getListByParent($parentid, $siteid);
		$medias = array();
		foreach($sitemaps as $key =>$item)
		{
			$medias[$key] = $this->model_core_media->getItem($this->member->getSiteId().$item['sitemapid']);
			$medias[$key]['headertitle'] = $item['sitemapname'];
			$link = $this->document->createLink($item['sitemapid']);
				
			$imagethumbnailpng = "";
			$imagethumbnail = "";
			if($medias[$key]['imagepath'] != "" && $template['width'] >0 )
			{
				$imagethumbnailpng = HelperImage::resizePNG($medias[$key]['imagepath'], $template['width'], $template['height']);
				@$imagethumbnail = HelperImage::fixsize($medias[$key]['imagepath'], $template['width'], $template['height']);
			}
			$medias[$key]['imagethumbnailpng'] =$imagethumbnailpng;
			$medias[$key]['imagethumbnail'] =$imagethumbnail;
			$medias[$key]['summary'] = html_entity_decode($medias[$key]['summary']);
			$medias[$key]['link'] =$link;
		}
		$this->data['medias'] = $medias;
		$this->id="news";
		$this->template=$template['template'];
		$this->render();
		
	}
	
	public function showMenuContent($arrmenuid,$headername = "", $template = array())
	{
		$this->load->model('core/sitemap');	
		$this->load->model('core/media');	
		$this->load->helper('image');
		$siteid = $this->member->getSiteId();
		
		$where = "AND sitemapid in ('".implode("','",$arrmenuid)."')";
		$sitemaps = $this->model_core_sitemap->getList($siteid,$where);
		
		$medias = array();
		foreach($arrmenuid as $key =>$sitemapid)
		{
			
			$medias[$key] = $this->model_core_media->getItem($this->member->getSiteId().$sitemapid);
			$medias[$key]['headertitle'] = $item['sitemapname'];
			$link = $this->document->createLink($sitemapid);
				
			$imagethumbnail = "";
			if($medias[$key]['imagepath'] != "" && $template['width'] >0 )
			{
				$imagethumbnailpng = HelperImage::resizePNG($medias[$key]['imagepath'], $template['width'], $template['height']);
				@$imagethumbnail = HelperImage::fixsize($medias[$key]['imagepath'], $template['width'], $template['height']);
			}
			$medias[$key]['imagethumbnailpng'] =$imagethumbnailpng;
			$medias[$key]['imagethumbnail'] =$imagethumbnail;
			$medias[$key]['summary'] = html_entity_decode($medias[$key]['summary']);
			$medias[$key]['link'] =$link;
		}
		$this->data['medias'] = $medias;
		$this->id="news";
		$this->template=$template['template'];
		$this->render();
		
	}
	
	public function showContent($mediaid,$template = array())
	{
		$this->load->model("core/media");
		$this->load->helper('image');
		
		$this->data['media'] = $this->model_core_media->getItem($mediaid);
		$this->data['media']['imagethumbnail'] = HelperImage::fixsize($this->data['media']['imagepath'], $template['width'], $template['height']);
		$this->data['media']['description'] = html_entity_decode($this->data['media']['description']);
		$this->data['media']['link'] = $this->document->createLink(str_replace($this->member->getSiteId(),"",$mediaid));
		
		$this->id="news";
		$this->template=$template['template'];
		$this->render();
	}
	
	public function showRegisterPage($mediaid,$template = array())
	{
		$this->load->model("core/media");
		$this->load->model("core/sitemap");
		$this->load->helper('image');
		
		$this->data['media'] = $this->model_core_media->getItem($mediaid);
		$this->document->title .= $this->data['media']['title'];
		$this->data['media']['imagethumbnail'] = HelperImage::fixsize($this->data['media']['imagepath'], $template['width'], $template['height']);
		$this->data['media']['description'] = html_entity_decode($this->data['media']['description']);
		$this->data['media']['link'] = $this->document->createLink(str_replace($this->member->getSiteId(),"",$mediaid));
		
		$where = " AND moduleid = 'module/register' AND sitemapid <> 'dangkyonline'";
		$this->data['data_sitemap'] = $this->model_core_sitemap->getList($this->member->getSiteId(),$where);
		$this->id="news";
		$this->template=$template['template'];
		$this->render();
	}
	
	public function getProduct()
	{
		$this->load->model("core/product");
		$this->load->helper('image');
		$id=$this->request->get['id'];
		$product = $this->model_core_product->getItem($id);
		$product['accessoriesnames'] ="";
		if($product['accessories'])
		{
			$arr_accessories = split(",",$product['accessories']);
			$arr_accessoriesname = array();
			foreach($arr_accessories as $aid)
			{
				$acc = $this->model_core_product->getItem($aid);
				$arr_accessoriesname[] = $acc['productname'];	
			}
			$product['accessoriesnames'] = implode(" , ",$arr_accessoriesname);
		}
		
		
		$this->data['output'] = json_encode($product);
		$this->id="product-".$id;
		$this->template="common/output.tpl";
		$this->render();
	}
	
}
?>