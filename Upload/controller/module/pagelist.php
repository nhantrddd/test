<?php

class ControllerModulePagelist extends Controller

{

	public function getList($sitemapid="", $count = 0,$headername ="", $template = array(),$medias=array())

	{

		$this->load->model("core/media");

		$this->load->model("core/sitemap");

		$this->load->helper('image');

		$this->data['show_item'] = array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50);

		if($sitemapid == "")

			$sitemapid = $this->document->sitemapid;

		$siteid = $this->member->getSiteId();

		$this->data['sitemap'] = $this->model_core_sitemap->getItem($sitemapid, $siteid);

		$this->document->title .= " - ".$this->data['sitemap']['sitemapname'];

		$step = (int)$this->request->get['step'];
		
		$get_to = (int)$_GET['to'];
		if($get_to > 0){
			$to = $get_to;
		}
		else{
			$to = $count;
		}

		//Get list

		$child = array();

		$this->model_core_sitemap->getTreeSitemap($sitemapid,$child,$this->member->getSiteId());

		$listsitemap = array();

		if(count($child))

		{

			foreach($child as $item)

				$listsitemap[] = $item['sitemapid'];

		}

		

		$queryoptions = array();

		$queryoptions['mediaparent'] = '%';

		$queryoptions['mediatype'] = '%';

		$queryoptions['refersitemap'] = $listsitemap;

		$order = $_GET['order'];
		$orderby = "";
		switch($order)
		{
			case "az":
				$orderby = " ORDER BY `title` ASC";
				break;
			case "za":
				$orderby = " ORDER BY `title` DESC";
				break;
			case "new":
				$orderby = " ORDER BY `statusdate` DESC";
				break;
			case "old":
				$orderby = " ORDER BY `statusdate` ASC";
				break;
			/*case "gt":
				$orderby = " ORDER BY `price` ASC";
				break;
			case "gg":
				$orderby = " ORDER BY `price` DESC";
				break;*/
			default:
				$orderby = " Order by position, statusdate DESC";
		}

		if($mediaid == "")

		{

			

			

			if(count($medias) == 0)

			{

				$medias = $this->model_core_media->getPaginationList($queryoptions, $step, $to, $orderby);

			}

			

			$this->data['medias'] = array();

		

			$index = -1;

			foreach($medias as $media)

			{

				$index += 1;

				$arr = $this->string->referSiteMapToArray($media['refersitemap']);

				$sitemapid = $arr[0];

				$link = $this->document->createLink($sitemapid,$media['alias']);

				

				$imagethumbnail = "";

				//if($media['imagepath'] != "" && $template['width'] >0 )

				{

					$imagethumbnailpng = HelperImage::resizePNG($media['imagepath'], $template['width'], $template['height']);

					@$imagethumbnail = HelperImage::fixsize($media['imagepath'], $template['width'], $template['height']);
					
					$imagepreview = HelperImage::fixsize($media['imagepath'], $template['widthpreview'], $template['heightpreview']);

				}

	

				$startdate = $this->model_core_media->getInformation($media['mediaid'],"startdate");

				$this->data['medias'][] = array(

					'mediaid' => $media['mediaid'],

					'title' => $media['title'],

					'summary' => html_entity_decode($media['summary']),

					'startdate' => $this->date->formatMySQLDate($startdate),

					'eventdate' => $this->date->formatMySQLDate($media['eventdate']),

					'eventtime' => $media['eventtime'],

					'imagethumbnailpng' => $imagethumbnailpng,

					'imagethumbnail' => $imagethumbnail,

					'imagepreview' => $imagepreview,

					'statusdate' => $this->date->formatMySQLDate($media['statusdate'], 'longdate', "/"),

					'link' => $link

				);

				

			}

			

			$querystring = "?route=page/detail&sitemapid=".$sitemapid;

			

			$pagelinks = $this->model_core_media->getPaginationLinks($index, $queryoptions, $querystring, $step, $to);

			

			$this->data['nextlink'] = $pagelinks['nextlink'];

			$this->data['prevlink'] = $pagelinks['prevlink'];

			

			//Other news

			$this->data['othernews'] = $this->model_core_media->getPaginationList($queryoptions, $step+1, $to);

			for($i=0;$i<count($this->data['othernews']);$i++)

			{

				$this->data['othernews'][$i]['statusdate'] = $this->date->formatMySQLDate($this->data['othernews'][$i]['statusdate'], 'longdate', "/");

				$link = $this->document->createLink($sitemapid,$this->data['othernews'][$i]['alias']);

				$this->data['othernews'][$i]['link'] = $link;

			}

			

		}

		

		$this->id="news";

		$this->template=$template['template'];

		$this->render();

	

	}

	

	

}

?>