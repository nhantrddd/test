<?php
class ControllerPageHome extends Controller
{
	function __construct() 
	{
		//$this->iscache = true;
	 	$arr=array();
		foreach($_GET as $key => $val)
			$arr[] = $key."=".$val;
	 	$this->name ="PageHome".implode("_",$arr);
   	}
	public function index()
	{
		if($this->cachehtml->iscacht($this->name) == false)
		{	
			//$this->data['home'] = $this->loadModule("addon/home");
						
			//San pham saleoff
			$template = array(
						  'template' => "module/product_list.tpl",
						  'width' => 240,
						  'height' =>159,
						  'paging' => false,
						  'sorting' =>false
						  );
			
			$medias = $this->getProduct();
			$arr = array("",12,"",$template,$medias);
			$this->data['producthome'] = $this->loadModule('module/productlist','index',$arr);
			
			$this->loadSiteBar();
			$this->document->title = $this->document->setup['Title'] ." - ". $this->document->setup['Slogan'];
		}
		
		$this->id="content";
		$this->template="page/home.tpl";
		$this->layout="layout/home";
		$this->render();
	}
	
	private function loadSiteBar()
	{
		//Left sitebar
		$this->data['leftsitebar']['zone'] = $this->loadModule('sitebar/zone');
		$this->data['leftsitebar']['zone_nuocngoai'] = $this->loadModule('sitebar/zone', 'zone_nuocngoai');
		$this->data['leftsitebar']['supportonline'] = $this->loadModule('sitebar/supportonline');
		/*$arr = array('sanpham');
		$this->data['leftsitebar']['produtcategory'] = $this->loadModule('sitebar/catalogue','index',$arr);
		$this->data['leftsitebar']['supportonline'] = $this->loadModule('sitebar/supportonline');
		$this->data['leftsitebar']['exchange'] = $this->loadModule('sitebar/exchange');
		$this->data['leftsitebar']['weblink'] = $this->loadModule('sitebar/weblink');
		$this->data['leftsitebar']['hitcounter'] = $this->loadModule('sitebar/hitcounter');*/
		
		//Rigth sitebar
		/*$this->data['rightsitebar']['login'] = $this->loadModule('sitebar/login');
		$this->data['rightsitebar']['search'] = $this->loadModule('sitebar/search');
		$this->data['rightsitebar']['cart'] = $this->loadModule('sitebar/cart');
		$this->data['rightsitebar']['banner'] = $this->loadModule('sitebar/banner');
		$this->data['rightsitebar']['question'] = $this->loadModule('sitebar/question');*/
	}
	
	function getProduct()
	{
		$this->load->model('core/sitemap');
		$this->load->model('core/product');
		/*$siteid = $this->member->getSiteId();
		$sitemaps = $this->model_core_sitemap->getListByModule("module/product", $siteid);
		$arrsitemapid = $this->string->matrixToArray($sitemaps,"sitemapid");
		$queryoptions = array();
		$queryoptions['mediaparent'] = '%';
		$queryoptions['mediatype'] = '%';
		$options['refersitemap'] = $arrsitemapid;*/
		$data = $this->model_core_product->getList(" AND groupkeys LIKE '[saleoff]'");
		
		return $data;
	}
}
?>